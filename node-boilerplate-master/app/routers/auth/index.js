const router = require('express').Router();
const controllers = require('./lib/controllers');
const middleware = require('../../../globals/lib/middleware');

router.post('/signup', controllers.userSignup);
router.post('/signin', controllers.userSignin);
router.post('/adminsignin', controllers.adminSignin);
router.post('/logout', middleware.verifyUserToken, controllers.logOut);
router.post('/admin/logout', middleware.verifyAdminToken, controllers.logOut);

module.exports = router;
