pragma solidity ^0.8.0;

contract learnFunction{
    function multiplyCalculator(uint _a, uint _b)public view returns (uint){
        uint result = _a*_b;
        return result;
    }
}