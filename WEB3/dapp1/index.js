
var web3 = new Web3(window.ethereum);

const address = "0x0945aaf72be84B2d39d382f46bEF897cf0FEa5ac"

contract = new web3.eth.Contract(abi,address);

contract.methods.getBalance().call({from:address}, function(err,bal){
    try{
        document.getElementById("balance").innerHTML = bal;
    }catch(err){
        throw err;
    }

})

 function depositAmount (){
   var amount = document.getElementById("textbox").value;
    web3.eth.getAccounts().then(function (accounts){
        var _account = accounts[0];
        contract.methods.deposit(amount).send({from: _account});
    }).then(function(tx){
        console.log(tx)
    }).catch(function(tx){
        console.log(tx)
    })
}

 function withdrawAmount (){
    var amount = document.getElementById("textbox").value;
     web3.eth.getAccounts().then(function (accounts){
         var _account = accounts[0];
         contract.methods.withdraw(amount).send({from: _account});
     }).then(function(tx){
         console.log(tx)
     }).catch(function(tx){
         console.log(tx)
     })
 }


