const router = require('express').Router();

const adminRoute = require('./admin_render');
const userRoute = require('./user_render');

router.use('/a', adminRoute);
router.use('/', userRoute);

module.exports = router;