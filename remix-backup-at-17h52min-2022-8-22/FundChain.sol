//SPDX-License-Identifier: Rahul pithadia

pragma solidity ^0.8.13;

contract FundChain{
    struct Donor{
        string fullName;
        address donorAddress;
        string  Idproof;
    }

    struct NGO{
        string ngoName;
        string websiteURL;
        string ngoPanNumber;
        address ngoAddress;
    }

    struct Government{
        string ministryName ;
        string ministryDetails;
        address ministryAddress;
    }

    Government public government = Government("Ministry of social justices and walfare", "dmvidvjnx", 0x5B38Da6a701c568545dCfcB03FcB875f56beddC4);
    NGO [] public ngo;
    Donor [] donor;
    
     
}

