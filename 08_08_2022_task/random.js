const prompt = require("prompt-sync")();
class node {
  // creating an empty node
  constructor(value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {
  // setting up linkedlist class to define it intances like head, tail and length.
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }

  insertAtEnd() {

    let value = prompt("Enter the value you want in -> ");// Taking input from user.
    console.log("\n");
    this.length++;
    let newNode = new node(value);

    if (this.tail) {
      //setting up pointer's postion to insert value at the end.
      this.tail.next = newNode;
      this.tail = newNode;
      return newNode;
    }

    return this.head = this.tail = newNode; //letting postion of pointer at same side so that it wont
                                            // overwrite previous node for add new node at the end.
  }

  insertAtBeginning() {
    let value = prompt("Enter the data you want to insert in -> ");
    console.log("\n");

    this.lenght++;
    let newNode = new node(value);
    if (this.head) {
      // placing new empty node at beginning of the linkedlist and moving current node at 1st position.
      newNode.next = this.head;
      this.head = newNode;
      return newNode;
    }

    return this.head = this.tail = this.newNode
  }

  insertAtIndex() {
    let index = prompt("Enter the index you  want to insert at -> ");
    let value = prompt("Enter the data you want to Insert in -> ");
    //check if enterd index is within the range of array or not.
    if (index >= this.length+2) {
      throw new Error("Insert index out of bounds");
    }
    if (index === 0) {
      return this.insertHead(value);
    }
    let previousNode = null;
    let currentNode = this.head;

    for (let i = 0; i < index; i++) {
      previousNode = currentNode;
      currentNode = currentNode.next;
    }
    let newNode = new node(value);
      // setting up a placing to inserting newnode by arranging previous and current node to newnode.
    newNode.next = currentNode;
    previousNode.next = newNode;

    this.length++
    return newNode;
  }

  removeAtEnd() {
    if (this.tail) {
      this.length--;

      const tailNode = this.tail;
          // search for the node before tail
      let currentNode = this.head;
        // The while loop stops when the node next to tail node is found
      while (currentNode.next != tailNode) {
        currentNode = currentNode.next;
      }

      const beforeTail = currentNode;
      this.tail = beforeTail;
      this.tail.next = null;

      return tailNode;
    }
    return undefined;

  }

  removeAtbeginning(){
    if (this.head) {
      this.length--;
      const removedNode = this.head;
      this.head = this.head.next;
      return removedNode;
    }
    return undefined;
  }




print() {
  let current = this.head;
  let printlist = []
  while (current) {
    printlist.push(current.value)
    //console.log(printlist.join(" -> ") );
    current = current.next;
  }
  console.log(printlist.join(" -> "));
}
}

const linkedList = new LinkedList();

console.log("Inserting data at the end of the linkedlist\n\n");
linkedList.insertAtEnd(); linkedList.insertAtEnd(); linkedList.insertAtEnd(); linkedList.print(); // 7 true 20

console.log("\n\nInserting data at the beginning of the linkedlist\n\n");
linkedList.insertAtBeginning(); linkedList.insertAtBeginning(); linkedList.insertAtBeginning(); linkedList.print(); // 7 true 20

console.log("\n\nInserting data at the specific postion of the linkedlist\n\n");
linkedList.insertAtIndex(); linkedList.print(); // 7 true 20

console.log("\n\n Removing data at the end of the linkedlist \n");
linkedList.removeAtEnd();linkedList.print();

console.log("\n\n Removing data at the beginning of the linkedlist")
linkedList.removeAtbeginning();linkedList.print();

