pragma solidity ^0.8.0;

import "@openzeppelin/contracts/security/PullPayment.sol";
contract DemoPullPayment is PullPayment{
    
    mapping (address => uint) private _balances;

     function deposit() public payable {
        require(msg.value != 0, "You need to deposit some amount of funds into wallet!");
        _balances[msg.sender] += msg.value;
    }
    function transfer(address payee, uint amount)public {
        _asyncTransfer(payee, amount);
    }
}