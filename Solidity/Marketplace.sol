// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/token/common/ERC2981.sol";

contract NFTTASK is ERC721, ERC721Enumerable, ERC721URIStorage, ERC721Burnable, Ownable, ERC2981 {
    using Counters for Counters.Counter;


    uint private tokenId;
    address private admin;
  //  address royaltyAddress;
    uint private adminFees = 0.3 ether;
   // uint royaltyFees;
    string public contractURI;

    mapping(address => uint) public balances;
    mapping(uint => uint) public price;

    modifier onlyAdmin(){
        require (msgSender() == admin, "Only admin address have a right to withdraw contracts token to its account.");
        _;
    } 

    function msgSender() public view returns(address) {
       return msg.sender;
    }
    

    constructor(uint96 _royaltyFees, string memory _contractURI) ERC721("Data Transfer Token", "DTT") {
        admin = msgSender();
        setRoyaltyInfo(owner(), _royaltyFees);
        contractURI = _contractURI;
    }

    function safeMint(string memory uri) public payable {
        require (msgSender()!= admin, "admin not allowed to mint its own NFT");
        require (msg.value == adminFees,"Sorry! insufficent amount try again with proper amount.");
        // tokenId = _tokenIdCounter.current();
        tokenId++;
        _safeMint(msgSender(), tokenId);
        _setTokenURI(tokenId, uri);
    }

    function setprice(uint _price, uint _tokenId) public {
        require(msgSender() == ownerOf(_tokenId), "Only Owner has a Rigth to decide a price of its token.");
        price[_tokenId]=_price;
    }

    function buy(uint _tokenId) public payable {
        require(msgSender() != ownerOf(_tokenId), "Owner can buy its Own token");
        require(msg.value == price[_tokenId],"Entered amount is not equal to price of token");
        (,uint256 amount)= royaltyInfo(_tokenId, price[_tokenId]);
        uint minterShare = msg.value - amount;
        balances[ownerOf(tokenId)]+= minterShare;
        balances[owner()] += (msg.value-minterShare);
        _transfer(ownerOf(_tokenId),msgSender(), tokenId);
        emit Transfer(ownerOf(_tokenId),msgSender(), tokenId);

    }

    function getContractbalances() public view returns(uint Amount) {
        Amount = address(this).balance;
    }

    function withdrawMintFees(uint amount) public onlyAdmin {
        require(amount <= address(this).balance);
        payable(admin).transfer(amount);
    }

    function setMintFees(uint amount) public onlyAdmin{
        adminFees = amount;
    }

    function getMintFees() public view returns(uint Wei){
        return adminFees;
    }

    function Admin() public view returns(address){
        return admin;
    }

    function setRoyaltyInfo(address _receiver, uint96 _royaltyFees) public onlyOwner{
        _setDefaultRoyalty(_receiver, _royaltyFees);
    }

    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        _burn(tokenId);
    }

    function tokenURI(uint256 tokenId)
        public
        view
        override(ERC721, ERC721URIStorage)
        returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable, ERC2981)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }

   
    


}