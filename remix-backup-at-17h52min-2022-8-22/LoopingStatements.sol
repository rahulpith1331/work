pragma solidity ^0.8.0;

contract loopingPractice{
uint [] longList = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
uint [] numberList = [1,4,32,56];

function loopNumberlist(uint _number)public view returns(bool){
    bool result;
    for(uint i=0; i<numberList.length; i++){
    if(numberList[i] == _number){
        result = true;
    }
 }
  return result;
   
}

function evenNumber()public view returns(uint){
    uint count = 0;
    for(uint i = 0; i < longList.length; i++){
        if(longList[i]%2 == 0){
            count ++;
        }
    }
    return count;
}

}

