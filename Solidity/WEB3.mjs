import Eth from 'web3-eth';

const  { givenProvider } = Eth;

// "Eth.providers.givenProvider" will be set if in an Ethereum supported browser.
var eth = new Eth(givenProvider || 'ws://some.local-or-remote.node:8546');

console.log(eth.getAccounts(console.log));
// or using the web3 umbrella package
