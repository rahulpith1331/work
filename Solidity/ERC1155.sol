//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155Receiver.sol";
import "@openzeppelin/contracts/token/ERC1155/extensions/IERC1155MetadataURI.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


contract ERC1155 is Context,IERC1155, ERC165, IERC1155MetadataURI {
    using Address for address;
    using Strings for uint256;

     bytes private _json =".json";
    uint private _tokenid;
    uint[] private _batchtokenid;
    string private _uri;

    mapping (uint => mapping (address => uint)) private _balances;
    mapping(address => mapping(address => bool)) private _operatorApprovals;
    mapping(uint => string) private _tokenURIs;
     

    constructor(string memory uri_){
        _uri = uri_;

    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC165, IERC165) returns (bool) {
        return interfaceId == type(IERC1155).interfaceId ||
            interfaceId == type(IERC1155MetadataURI).interfaceId ||
            super.supportsInterface(interfaceId);
    }

    // interface methods.

    function balanceOf(address _owner, uint256 _id) public view override  returns (uint256){
        require(msg.sender != address(0), "ERC1155: Address can't be Zero");
        return  _balances[_id][_owner];
    }

    function balanceOfBatch(address[] calldata _owner, uint [] calldata _ids) public view override  returns (uint[] memory){
        require(msg.sender != address(0),"ERC1155: Address can't be Zero");
         uint256[] memory batchBalances = new uint256[](_owner.length);

        for(uint i = 0; i< _ids.length; i++){
            batchBalances[i] = balanceOf(_owner[i], _ids[i]);
        }
        return batchBalances;
    }

    function safeTransferFrom(address _from, address _to, uint256 _id, uint256 _value, bytes calldata _data) public override {
        require(_from == _msgSender() || isApprovedForAll(_from, _msgSender()), "ERC1155: caller is not token owner or approved");
        _safeTranferFrom(_from, _to, _id, _value, _data);
    }

    
    function safeBatchTransferFrom(address _from, address _to, uint256[] calldata _ids, uint256[] calldata _values, bytes calldata _data) public override{
          require(_from == _msgSender() || isApprovedForAll(_from, _msgSender()), "ERC1155: caller is not token owner or approved");
          _safeBatchTranferFrom(_from, _to, _ids, _values, _data);
    }

     function setApprovalForAll(address _operator, bool _approved) public override {
         require(_operator != _msgSender(),"ERC1155: setting approval status for self");
         _operatorApprovals[_msgSender()][_operator] = _approved;
         emit ApprovalForAll(_msgSender(), _operator, _approved);

     }

     function isApprovedForAll(address account, address operator) public view  override returns (bool) {
        return _operatorApprovals[account][operator];
    }

      function uri(uint256 _id) public view override returns (string memory) {
        return _tokenURIs[_id];
    }


    function mint (uint _amount) public {
        _tokenid++;
        string memory token_id = Strings.toString(_tokenid);
        bytes memory uri_ = bytes(_uri);
        bytes memory tokenid_ = bytes(token_id);
        bytes  memory seturi = bytes.concat(uri_,tokenid_ ,_json);
        string memory seturi_ =string(seturi);
        _mint(_msgSender() , _tokenid, _amount, "");
        _seturi(_tokenid, seturi_);
    }

    function mintBatch( uint[] memory _amounts) public {
       for (uint i = 0 ; i< _amounts.length; i++){
            _tokenid++;
            string memory seturi =  string(abi.encodePacked(_uri, Strings.toString(_tokenid), _json));
            _seturi(_tokenid, seturi);
            _batchtokenid.push(_tokenid);
            
       }
        _mintBatch(_msgSender(), _batchtokenid, _amounts, "");
    }



    // Internal function / Private function.


    function _mint(address _to, uint _id, uint _amount, bytes memory _data ) internal virtual{
        require(_to != address(0), "ERC1155: minting to zerio address.");
        address _operator = _msgSender();
        _balances[_id][_to] += _amount;
         _doSafeTransferAcceptanceCheck(_operator, address(0), _to, _id, _amount, _data);
        emit TransferSingle(_operator, address(0), _to, _id, _amount);
    }

    function _mintBatch(address _to, uint[] memory  _ids, uint[] memory _amounts, bytes memory _data) internal virtual {
        require(_to != address(0), "ERC1155: minting to zerio address.");
        require(_ids.length == _amounts.length, "ERC1155: ids and amount length mismatch");
         
         address _operator = _msgSender();

        for(uint i=0; i< _ids.length; i++){
            _balances[_ids[i]][_to]+= _amounts[i];
        }
        _doSafeBatchTransferAcceptanceCheck(_operator, address(0), _to, _ids, _amounts, _data);
        emit TransferBatch(_operator, address(0), _to, _ids, _amounts);

    }

    function _safeTranferFrom(address _from, address _to, uint _id, uint _amount, bytes memory _data)internal virtual{
         require(_to != address(0), "ERC1155: minting to zerio address.");
         require(_balances[_id][_from] >= _amount,"ERC1155: insufficient balance for transfer");

        address _operator = _msgSender();

         _balances[_id][_from]-= _amount;
         _balances[_id][_to]+=_amount;
         emit TransferSingle(_operator, _from, _to, _id, _amount);
         _doSafeTransferAcceptanceCheck(_operator, _from, _to, _id, _amount, _data);
    }

    function _safeBatchTranferFrom(address _from, address _to, uint[] memory _ids, uint[] memory _amounts, bytes memory _data) internal virtual{
        require(_to != address(0), "ERC1155: minting to zerio address.");
        require(_ids.length == _amounts.length, "ERC1155: ids and amount length mismatch");
        address operator = _msgSender();
        for(uint i = 0; i < _ids.length; i++){

            uint ids = _ids[i];
            uint amount = _amounts[i];

            require(_balances[ids][_from] >= amount, "ERC1155: insufficient balance for transfer");

            _balances[ids][_from] -= amount;
            _balances[ids][_to] += amount;
        }

        emit TransferBatch(operator, _from, _to, _ids, _amounts);
        _doSafeBatchTransferAcceptanceCheck(operator, _from, _to, _ids, _amounts, _data);
    }

    function _doSafeTransferAcceptanceCheck(address _operator, address _from, address _to, uint _id, uint _amount, bytes memory _data) private {
        if(_to.isContract()){
            try IERC1155Receiver(_to).onERC1155Received(_operator, _from, _id, _amount, _data) returns (bytes4 response){
                if(response != IERC1155Receiver.onERC1155Received.selector){
                    revert("ERC1155: ERC1155Receiver rejected tokens");
                }    
            }catch Error(string memory reason){
                revert(reason);
            }catch{
                 revert("ERC1155: transfer to non-ERC1155Receiver implementer");
            }
        }
    }

    function _doSafeBatchTransferAcceptanceCheck(address operator, address from, address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data ) private {
        if (to.isContract()) {
            try IERC1155Receiver(to).onERC1155BatchReceived(operator, from, ids, amounts, data) returns (
                bytes4 response
            ) {
                if (response != IERC1155Receiver.onERC1155BatchReceived.selector) {
                    revert("ERC1155: ERC1155Receiver rejected tokens");
                }
            } catch Error(string memory reason) {
                revert(reason);
            } catch {
                revert("ERC1155: transfer to non-ERC1155Receiver implementer");
            }
        }
    }

    function _seturi(uint tokenid, string memory tokenuri) internal virtual {
        _tokenURIs[tokenid] = tokenuri;
        emit URI(uri(tokenid), tokenid);
    }

}