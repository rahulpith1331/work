const router = require('express').Router();
const controllers = require('./lib/controllers');
const adminMiddleware = require('../../../globals/lib/middleware');

router.post(
    '/users',
    adminMiddleware.verifyAdminToken,
    controllers.getUserData
);
router.patch(
    '/updateuserstatus',
    adminMiddleware.verifyAdminToken,
    controllers.setAccountStatus
);

module.exports = router;
