/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const web3 = new Web3(window.ethereum);

const notyf = new Notyf({
    duration: 4000,
    position: {
        x: 'right',
        y: 'top',
    },
});

$(function () {
    const sToken = localStorage.getItem('sAuthToken');
    const sWalletAddress = localStorage.getItem('sWalletAddress');
    const sRole = localStorage.getItem('sRole');
    if (sToken && sRole) {
        notyf.error('Please first logout from admin account');
        let tID;
        return (tID = setTimeout(function () {
            location.href = '/a/userslist';
            window.clearTimeout(tID); // clear time out.
        }, 2000));
    }

    if (sToken && sWalletAddress) {
        location.href = '/userProfile';
    }
});

// conneting metamask
const connectMetamask = async () => {
    try {
        if (window.ethereum !== 'undefined') {
            const sAccount = await window.ethereum.request({
                method: 'eth_requestAccounts',
            });
            if (!sAccount) {
                return;
            } else {
                // $('#wallet-address').html(sAccount);
                // $('#connectButton').hide();

                const sWalletAddress = sAccount[0];
                // console.log(sWalletAddress);

                $.ajax({
                    type: 'POST',
                    url: '/api/v1/user/checkuser',
                    data: { sWalletAddress },
                    success: function (result, status, xhr) {
                        console.warn(xhr);
                        $('#sigInModalCenter').modal('show');
                        $('#wallet-Address').html(sWalletAddress);
                        // $('#disconnectButton').show();
                    },
                    error: function (xhr) {
                        console.warn(xhr);
                        $('#signUpModalCenter').modal('show');
                        //  $('#disconnectButton').show()
                        $('#walletaddress-inp').val(sWalletAddress);
                    },
                });
            }
        } else {
            notyf.error('Metamask not installed on this Browser');
        }
    } catch (err) {
        notyf.error('You have reject the connect request from site');
    }
};

function closeSignin() {
    $('#sigInModalCenter').modal('hide');
}

function closeSignup() {
    $('#signUpModalCenter').modal('hide');
}

function signIn() {
    // console.log('hello from signin');
    const sWalletAddress = $('#wallet-Address').html().trim();

    signMessage(sWalletAddress);

    // console.log(sWalletAddress);
}

function signUp() {
    // console.log('hello from signup');
    const sUsername = $('#username-inp').val().trim();
    const sEmail = $('#email-inp').val().trim();
    const sWalletAddress = $('#walletaddress-inp').val().trim();
    const sPassword = $('#password-inp').val().trim();
    // console.log(sWalletAddress, sUsername, sEmail, sPassword);

    $.ajax({
        type: 'POST',
        url: '/api/v1/auth/signup',
        data: { sUsername, sWalletAddress, sEmail, sPassword },
        success: function (result, status, xhr) {
            notyf.success(xhr.responseJSON.message);
            //  window.localStorage.setItem('User', xhr.responseJSON.User);
            window.localStorage.setItem('sWalletAddress', sWalletAddress);
            $('#signUpModalCenter').modal('hide');
            $('#wallet-Address').html(sWalletAddress);
            $('#sigInModalCenter').modal('show');
            window.location.href = '/';
        },
        error: function (xhr) {
            notyf.error(xhr.responseJSON.message);

            const tID = setTimeout(function () {
                location.href = '/';
                window.clearTimeout(tID); // clear time out.
            }, 2000);
        },
    });
}

async function signMessage(sWalletAddress) {
    sMessage =
        'Message: the quick fox jump over the lazy dog\n' +
        'TimeStamp: ' +
        Date.now();

    const sSignature = await web3.eth.personal.sign(sMessage, sWalletAddress);

    // console.log(sMessage);

    $.ajax({
        type: 'POST',
        url: '/api/v1/auth/signin',
        data: { sWalletAddress, sMessage, sSignature },
        success: function (result, status, xhr) {
            console.warn(xhr);
            notyf.success(xhr.responseJSON.message);
            const tID = setTimeout(function () {
                location.href = '/userProfile';
                window.clearTimeout(tID); // clear time out.
            }, 2000);
            window.localStorage.setItem(
                'sAuthToken',
                xhr.responseJSON.data.data
            );
            window.localStorage.setItem('sWalletAddress', sWalletAddress);
            $('#sigInModalCenter').modal('hide');
        },
        error: function (xhr) {
            notyf.error(xhr.responseJSON.message);
            const tID = setTimeout(function () {
                location.href = '/';
                window.clearTimeout(tID); // clear time out.
            }, 2000);
        },
    });
}
