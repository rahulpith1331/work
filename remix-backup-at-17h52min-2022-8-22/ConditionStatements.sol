pragma solidity ^0.8.0;

contract ifEsleStatement {
    uint stackingWallet =10;

    function airDrop() public view returns (uint){
        if(stackingWallet == 10){
           return stackingWallet + 10;
        }
        else{
          return stackingWallet+1;
        }
    }
}