function keyformatter(str, k) {
    str = str.trim().replace(/[^a-zA-Z0-9]/g, "").toUpperCase().split("");
       let len = str.length;
    for (let i = len; i > 0; i = i - k) {
        if (i != len) {
            str[i - 1] = str[i - 1] + "-";
        }
    }
    return str.join("");
}

console.log(keyformatter("2-5g-3-J", 2))
console.log(keyformatter("5F3Z-2e-9-w", 4))

