/* eslint-disable no-undef */
const notyf = new Notyf({
    duration: 4000,
    position: {
        x: 'right',
        y: 'top',
    },
});

$(function () {
    const sToken = localStorage.getItem('sAuthToken');
    const sWalletAddress = localStorage.getItem('sWalletAddress');
    const sRole = localStorage.getItem('sRole');

    if (sToken && sWalletAddress) {
        return (location.href = '/userprofile');
    }

    if (sToken && sRole) {
        return (location.href = '/a/userdata');
    }
});

// eslint-disable-next-line no-unused-vars
function signIn() {
    // eslint-disable-next-line no-undef
    const sEmail = $('#admin-email').val().trim();
    const sPassword = $('#admin-password').val().trim();

    $.ajax({
        type: 'POST',
        url: '/api/v1/auth/adminsignin',
        data: { sEmail, sPassword },
        success: function (result, status, xhr) {
            // console.log(xhr)
            notyf.success(xhr.responseJSON.message);
            const tID = setTimeout(function () {
                window.location.href = '/a/userdata';
                window.clearTimeout(tID); // clear time out.
            }, 2000);
            localStorage.setItem('sAuthToken', xhr.responseJSON.data.data);
            localStorage.setItem('sRole', xhr.responseJSON.sRole);
        },

        error: function (xhr, result, status) {
            
            // log.warn(status);
            if (status === 'Bad Request') {
                $('#p-email').show();
                $('#p-email').html(xhr.responseJSON.message);
            } else if (status === 'Unauthorized') {
                $('#p-password').show();
                $('#p-password').html(xhr.responseJSON.message);
            } else {
                // $.notify(xhr.responseJSON.message, 'error');
                notyf.error(xhr.responseJSON.message);
            }
        },
    });
}
