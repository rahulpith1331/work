const router = require('express').Router();
const controllers = require('./lib/controller');

router.get('/signin', controllers.adminsignin);
router.get('/userdata', controllers.usersData);

module.exports = router;
