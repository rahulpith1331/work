const { MerkleTree } = require('merkletreejs');
const keccak256 = require('keccak256');
const util = require('ethereumjs-util');
const { keccak } = require('ethereumjs-util');

const addresses = [
    '0x4B20993Bc481177ec7E8f571ceCaE8A9e22C02db',
    '0x5B38Da6a701c568545dCfcB03FcB875f56beddC4',
    '0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2',
    '0x78731D3Ca6b7E34aC0F824c42a7cC18A495cabaB',
    '0x617F2E2fD72FD9D5503197092aC168c91465E7f2',
    '0x17F6AD8Ef982297579C203069C1DbfFE4348c372',
];

// leaves
const leaves = addresses.map((address) => keccak256(address));

// Creating tree
const tree = new MerkleTree(leaves, keccak256, { sortPairs: true });
let root = tree.getRoot();
root = '0x' + root.toString('hex');

console.log(tree.toString());

console.log('ROOT===> ', root);

let address = '0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2';
let leaf = keccak256(address);
const finalleaf = util.bufferToHex(leaf);
let proof = tree.getHexProof(leaf);
console.log('LEAF=====> ', finalleaf);
console.log('PROOF=====> ', proof);
