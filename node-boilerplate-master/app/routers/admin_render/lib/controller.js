const controller = {};

controller.adminsignin = (req, res) => {
    res.render('adminview/admin');
};

controller.usersData = (req, res) => {
    res.render('adminview/Userdata');
};

module.exports = controller;
