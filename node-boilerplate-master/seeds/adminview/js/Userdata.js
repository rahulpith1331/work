/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const notyf = new Notyf({
    duration: 4000,
    position: {
        x: 'right',
        y: 'top',
    },
});

$(function () {
    const sToken = localStorage.getItem('sAuthToken');
    const sWalletAddress = localStorage.getItem('sWalletAddress');

    if (sToken && sWalletAddress) {
        notyf.error('Please first logout from user account');
        let tID;
        return (tID = setTimeout(function () {
            location.href = '/userprofile';
            window.clearTimeout(tID); // clear time out.
        }, 1000));
    }

    window.addEventListener('storage', myFunction);

    function myFunction(event) {
        const sToken = localStorage.getItem('sAuthToken');

        if (event.oldValue) {
            $.ajax({
                type: 'POST',
                url: '/api/v1/auth/logOut',
                data: {},
                headers: { authorization: sToken },
                success: function (result, status, xhr) {
                    localStorage.clear();
                    const tID = setTimeout(function () {
                        window.location.href = '/a/signin';
                        window.clearTimeout(tID); // clear time out.
                    }, 1000);
                },

                error: function (xhr) {
                    notyf.error(xhr.responseJSON.message);
                },
            });
        }
    }

    $('#logout').show();
    $('#nftTable').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            type: 'POST',
            url: '/api/v1/admin/users',
            dataFilter: function (data) {
                const json = jQuery.parseJSON(data);

                const oData = {};
                oData.data = json.data.data;
                oData.draw = json.data.draw;
                oData.recordsFiltered = json.data.recordsFiltered;
                oData.recordsTotal = json.data.recordsTotal;

                return JSON.stringify(oData);
            },

            headers: { authorization: sToken },
        },

        aoColumns: [
            {
                mData: 'sUsername',
            },
            {
                mData: 'sEmail',
            },
            {
                mData: 'sWalletAddress',
            },
            {
                mData: 'sAccountStatus',
                orderable: false,
            },
            {
                mData: 'sAccountStatus',
                orderable: false,
                render: function (mData, type, row) {
                    return `<i class="bi ${
                        mData === 'block'
                            ? 'btn bi-unlock-fill text-success'
                            : 'btn bi-lock-fill text-danger'
                    }"  onclick="statusUpdate('${row.sWalletAddress}','${
                        row.sAccountStatus
                    }')">
                            </i>`;
                },
            },
        ],
        columnDefs: [
            {
                searchable: true,
                orderable: true,
                // targets: 0,
            },
        ],
    });
});

function callAPI(sType, oData, sUrl, oHeader) {
    return $.ajax({
        type: sType,
        url: sUrl,
        data: oData,
        headers: oHeader,
    });
}

async function statusUpdate(sWalletAddress, sAccountStatus) {
    try {
        const sToken = localStorage.getItem('sAuthToken');
        await callAPI(
            'PATCH',
            {
                sWalletAddress,
                sAccountStatus:
                    sAccountStatus === 'unblock'
                        ? (sAccountStatus = 'block')
                        : (sAccountStatus = 'unblock'),
            },
            '/api/v1/admin/updateuserstatus',
            { authorization: sToken }
        );

        $('#nftTable').DataTable().ajax.reload();
    } catch (err) {

        notyf.error('internal server error');
    }
}

function logOut() {
    const sToken = localStorage.getItem('sAuthToken');
    $.ajax({
        type: 'POST',
        url: '/api/v1/auth/admin/logout',
        data: {},
        headers: { authorization: sToken },
        success: function (result, status, xhr) {
            notyf.success(xhr.responseJSON.message);
            localStorage.clear();
            const tID = setTimeout(function () {
                window.location.href = '/a/signin';
                window.clearTimeout(tID); // clear time out.
            }, 1000);
        },

        error: function (xhr) {
            notyf.error(xhr.responseJSON.message);
        },
    });
}
