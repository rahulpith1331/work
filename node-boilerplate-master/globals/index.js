global.emitter = require('./lib/emitter');
global._ = require('./lib/helper');
global.log = require('./lib/log');
global.messages = require('./lib/message');
global.timer = require('./lib/timer');
global.adminMiddleware = require('./lib/middleware').verifyAdminToken;
global.userMiddleware = require('./lib/middleware').verifyUserToken;
