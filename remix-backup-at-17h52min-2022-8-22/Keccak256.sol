pragma solidity ^0.8.0;

contract Crypto{

    address owner = msg.sender;

    function gethash(string memory _message )public view returns(bytes32){
        
        return sha256(abi.encodePacked(_message, owner));
    }
}