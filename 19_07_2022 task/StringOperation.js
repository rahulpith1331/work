function stringOperation(){
    let String = "Hello! I'm a new member of the Raw Team. my name is Rahul";
console.log(String+"\n")

    // 1. Lenght of String.
    console.log("1) Length of above given string is -> "+String.length);

    //2. slicing or diving into diffent parts.
    console.log("2) Diving above string into two parts -> "+String.slice(0, 6)+ String.slice(34, 57))
    
    //3. Replacing content of String.
    console.log("3) Replacing string content -> "+String.replace("Raw", "Blockchain"));

    //4. Converting string into uppercase & lowercase.
    console.log("4) Converting above given string into uppercase -> " +String.toUpperCase());
    console.log("4.1) Converting above given string into lowercase -> " +String.toLowerCase());

    //5. Merging two diffrent strings.
    let String1 =" Pithadia"
    console.log("5) Merging two string ->"+String+String1);
    
    //6. Comparing two string    
    console.log("6) Comparing above given string -> "+String.localeCompare(String1));


}

stringOperation();