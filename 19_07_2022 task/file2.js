const fs = require('fs');
  
// Get the current filenames
// before the function
getCurrentFilenames();
console.log("\nFile Contents of hello.txt:",
      fs.readFileSync("sample.txt", "utf8"));
  
fs.copyFileSync("sample.txt", "sample1.txt");
  
// Get the current filenames
// after the function
getCurrentFilenames();
console.log("\nFile Contents of world.txt:",
      fs.readFileSync("sample1.txt", "utf8"));
  
// Function to get current filenames
// in directory
function getCurrentFilenames() {
  console.log("\nCurrent files in directory:");
  fs.readdirSync(__dirname).forEach(file => {
    console.log(file);
  });
}