/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
const notyf = new Notyf({
    duration: 4000,
    position: {
        x: 'right',
        y: 'top',
    },
});

const web3 = new Web3(window.ethereum);

$(() => {
    const sRole = localStorage.getItem('sRole');
    const sToken = localStorage.getItem('sAuthToken');
    const sWalletAddress = localStorage.getItem('sWalletAddress');

    if (sToken && sRole) {
        notyf.error('Please first logout from admin account');
        let tID;
        return (tID = setTimeout(function () {
            location.href = '/a/signin';
            window.clearTimeout(tID); // clear time out.
        }, 1000));
    }

    window.addEventListener('storage', myFunction);

    function myFunction(event) {
        const sToken = localStorage.getItem('sAuthToken');
        console.warn(event.oldValue);
        if (event.oldValue) {
            $.ajax({
                type: 'POST',
                url: '/api/v1/auth/logout',
                data: {},
                headers: { authorization: sToken },
                success: function (result, status, xhr) {
                    localStorage.clear();
                    window.location.href = '/';
                },

                error: function (xhr) {
                    notyf.error(xhr.responseJSON.message);
                    const tID = setTimeout(function () {
                        location.href = '/';
                        window.clearTimeout(tID); // clear time out.
                    }, 1000);
                },
            });
        }
    }

    $('#disconnectButton').show();
    $('#connectButton').hide();
    $('#wallet-address').html(sWalletAddress);

    $.ajax({
        type: 'POST',
        url: '/api/v1/user/getuser',
        data: {},
        headers: { authorization: sToken },
        success: function (result, status, xhr) {
            console.warn(xhr);
            $('#edit-button').show();
            $('#username').html(
                `Username: ${xhr.responseJSON.data.data[0].sUsername}`
            );
            $('#email').html(`Email: ${xhr.responseJSON.data.data[0].sEmail}`);
            $('#walletaddress').html(
                `Wallet Address: ${xhr.responseJSON.data.data[0].sWalletAddress}`
            );
            $('#accountstatus').html(
                `Account Status: ${xhr.responseJSON.data.data[0].sAccountStatus}`
            );
            $('#username-inp').val(xhr.responseJSON.data.data[0].sUsername);
            $('#email-inp').val(xhr.responseJSON.data.data[0].sEmail);
        },
        error: function (xhr) {
            localStorage.clear();

            notyf.error(xhr.responseJSON.message);

            const tID = setTimeout(function () {
                location.href = '/';
                window.clearTimeout(tID); // clear time out.
            }, 1000);
        },
    });
});

const disconnectMetamask = () => {
    const sToken = localStorage.getItem('sAuthToken');
    localStorage.clear();

    $.ajax({
        type: 'POST',
        url: '/api/v1/auth/logout',
        data: {},
        headers: { authorization: sToken },
        success: function (result, status, xhr) {
            notyf.success(xhr.responseJSON.message);

            localStorage.clear();

            const tID = setTimeout(function () {
                location.href = '/';
                window.clearTimeout(tID); // clear time out.
            }, 1000);
        },
        error: function (xhr) {
            notyf.error(xhr.responseJSON.message);

            const tID = setTimeout(function () {
                location.href = '/';
                window.clearTimeout(tID); // clear time out.
            }, 1000);

            localStorage.clear();
        },
    });
};

function editProfile() {
    const sUsername = $('#username-inp').val().trim();
    const sUsernameValidate = $('p-username').get();
    const sEmail = $('#email-inp').val().trim();
    const sEmailValidate = $('#p-email').get();

    console.warn(sUsername, sEmail);

    if (!emptyInputValidation(sUsername, sUsernameValidate)) return;

    if (!emptyInputValidation(sEmail, sEmailValidate)) return;

    sign(sUsername, sEmail);
}

function emptyInputValidation(sInput, sErrorMsg) {
    if (sInput.length == 0) {
        $(sErrorMsg).html('Sorry! we cannot proceed with empty field');
        $(sErrorMsg).show();
        return false;
    } else {
        return true;
    }
}

async function sign(sUsername, sEmail) {
    const sToken = localStorage.getItem('sAuthToken');
    const sWalletAddress = localStorage.getItem('sWalletAddress');

    const domain = [
        { name: 'name', type: 'string' },
        { name: 'version', type: 'string' },
        { name: 'chainId', type: 'uint256' },
        { name: 'verifyingContract', type: 'address' },
    ];
    const editprofile = [
        { name: 'username', type: 'string' },
        { name: 'email', type: 'string' },
        { name: 'timestamp', type: 'uint256' },
    ];

    const domainData = {
        name: 'My amazing dApp',
        version: '2',
        chainId: 97,
        verifyingContract: '0x55bad63D41AD3C242370edE7Ef8BE93230954fcb',
    };

    const message = {
        username: sUsername,
        email: sEmail,
        timestamp: Date.now(),
    };

    const sData = {
        types: {
            EIP712Domain: domain,
            Edit: editprofile,
        },
        domain: domainData,
        primaryType: 'Edit',
        message: message,
    };

    console.warn(JSON.stringify(sData));

    const sSignature = await window.ethereum.request({
        method: 'eth_signTypedData_v3',
        params: [sWalletAddress, JSON.stringify(sData)],
        from: sWalletAddress,
    });

    $.ajax({
        type: 'PATCH',
        url: '/api/v1/user/editprofile',
        data: {
            sUsername,
            sEmail,
            sSignature,
            sData,
        },
        headers: { authorization: sToken },
        success: function (result, status, xhr) {
            console.warn(xhr);
            location.reload();
        },
        error: function (xhr) {
            notyf.error(xhr.responseJSON.message);

            localStorage.clear();

            const tID = setTimeout(function () {
                // location.href = '/';
                window.clearTimeout(tID); // clear time out.
            }, 1000);
        },
    });

    const address = await web3.eth.personal.ecRecover(
        JSON.stringify(sData),
        sSignature
    );
    console.warn(address);
}
