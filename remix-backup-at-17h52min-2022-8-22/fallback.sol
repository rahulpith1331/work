pragma solidity ^0.8.0;

contract fallbacks{

    event log(uint gas);

    fallback() external payable{
        emit log(gasleft());
    }
    function getbalance() public view returns(uint){
        return address(this).balance;
    }
}

contract SendToFallBack {
    
    function transferToFallBack(address payable _to) public payable {
        // send ether with the transfer method
        // automatically transfer will transfer 2300 gas amount 
        _to.transfer(msg.value);
    }
    
    
    function callFallBack(address payable _to) public payable {
        // send ether with the call method 
        (bool sent,) = _to.call{value:msg.value}('');
        require(sent, 'Failed to send!');
    }
    
}