pragma solidity ^0.8.0;

contract StringLearning{

    string favoritecolor ="blue";

    function printcolor()public view returns(string memory){
        return favoritecolor;
    }

    function changefavoritecolor(string memory _change) public {
        favoritecolor = _change;
    }

    function getstringLength() public view returns (uint){
        return favoritecolor.length;
      // bytes memory lengthOfFavoriteColor = bytes(favoritecolor);
        //return lengthOfFavoriteColor.length; 
       }
}