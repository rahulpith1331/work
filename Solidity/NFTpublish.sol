// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Burnable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/utils/Strings.sol";


contract Cartoon is ERC721, ERC721Enumerable, ERC721URIStorage, ERC721Burnable, Ownable {
    using Counters for Counters.Counter;
    using Strings for uint256;

    uint private tokenId;
     uint256  MAX_SUPPLY = 3;
     bytes private uri = "ipfs://QmaZ77T6X2H1SnDPAY1aYQ5xVno5zfnF29ZAAA8L7K9cyW/";
     bytes json =".json";
     address msgsender = msg.sender;
     uint private metacounter = 3;

    constructor() ERC721("Catoons", "CTS") {}

    
   
    function safeMint() public {
        // increment();
        require(tokenId < MAX_SUPPLY, "Max supply reached");
        // tokenId.increment();
        tokenId++;
        string memory token_Id = Strings.toString(tokenId);
        bytes memory tokenId_ = bytes(token_Id);
        bytes  memory seturi = bytes.concat(uri,tokenId_ ,json);
        string memory seturi_ =string(seturi);
        _safeMint(msgsender, tokenId);
        _setTokenURI(tokenId, seturi_);
    }

    // The following functions are overrides required by Solidity.

    function _beforeTokenTransfer(address from, address to, uint256 tokenId)
        internal
        override(ERC721, ERC721Enumerable)
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }

    function _burn(uint256 tokenId) internal override(ERC721, ERC721URIStorage) {
        super._burn(tokenId);
    }

    function tokenURI(uint256 tokenId) public view override(ERC721, ERC721URIStorage) returns (string memory)
    {
        return super.tokenURI(tokenId);
    }

    function supportsInterface(bytes4 interfaceId)
        public
        view
        override(ERC721, ERC721Enumerable)
        returns (bool)
    {
        return super.supportsInterface(interfaceId);
    }
}