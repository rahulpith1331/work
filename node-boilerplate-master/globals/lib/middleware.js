const jwt = require('jsonwebtoken');
require('dotenv').config();

const verifyAdminToken = (req, res, next) => {
    try {
        // Authorization

        console.warn(req.session);
        if (!req.session.sUserId) {
            return res
                .status(419)
                .json({ message: 'Please signin with valid credentials' });
        }
        // req.headers.authorization

        const token = req.headers['authorization'];

        if (token == 'null') {
            return res
                .status(403)
                .json({ message: 'A token is required for authentication' });
        }

        try {
            const oDecoded = jwt.verify(token, process.env.TOKEN_KEY);
            req.user = oDecoded;
            log.warn();
            req.user;

            if (req.user.sRole !== 'admin') {
                // Message: Authorization failed.
                return res.reply(messages.unauthorized());
            }
        } catch {
            return res.reply(messages.wrong_credentials());
        }
        return next();
    } catch (err) {
        log.warn(err);
        return res.reply(messages.error('internal server'));
    }
};

const verifyUserToken = (req, res, next) => {
    try {
        // Authorization
        if (!req.session.sUserId) {
            return res.reply(messages.unauthorized());
        }
        // req.headers.authorization

        const token = req.headers['authorization'];
        log.warn(token);

        if (token == 'null') {
            req.session.destroy();
            return res
                .status(403)
                .json({ message: 'A token is required for authentication' });
        }
        try {
            const oDecoded = jwt.verify(token, process.env.TOKEN_KEY);
            req.user = oDecoded;
            log.warn(req.user);

            if (
                req.user.user_id !== req.session.sUserId &&
                req.user.sRole !== 'user'
            ) {
                // Message: Authorization failed.
                return res
                    .status(401)
                    .json({ message: 'Authorization failed.' });
            }
        } catch {
            return res.reply(messages.wrong_credentials());
        }
        return next();
    } catch (err) {
        log.warn(err);
        return res.reply(messages.error('internal server'));
    }
};

module.exports = { verifyAdminToken, verifyUserToken };
