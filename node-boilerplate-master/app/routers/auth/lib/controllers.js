const { Credential } = require('../../../models/index');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const sigUtil = require('eth-sig-util');

const controller = {};

controller.userSignup = async (req, res) => {
    try {
        const oExistUser = await Credential.findOne({
            sWalletAddress: req.body.sWalletAddress,
            sRole: 'user',
        });

        if (oExistUser) {
            return res.reply(messages.already_exists('user'));
        }

        const missing = [];

        if (!req.body.sEmail) {
            missing.push('Email');
        }

        if (!req.body.sPassword) {
            missing.push('Password');
        }

        if (!req.body.sUsername) {
            missing.push('Username');
        }

        if (!req.body.sWalletAddress) {
            missing.push('Wallet-Address');
        }

        if (missing.length) {
            return res.reply(
                messages.require_field(
                    missing.length ? missing.join(' ,') + 'are' : missing + 'is'
                )
            );
        }

        const encryptedPassword = await bcrypt.hash(req.body.sPassword, 10);

        const oUserData = {
            ...req.body,
            sPassword: encryptedPassword,
            sRole: 'user',
            sAccountStatus: 'unblock',
        };
        const oUser = new Credential(oUserData);
        const oError = oUser.validateSync();

        if (oError) {
            return res.status(406).json({ message: _.errorMessage(oError) });
        }

        // eslint-disable-next-line no-console
        log.warn(oUser);
        const aUser = new Credential(oUser);

        aUser.save();
        return res.reply(messages.successfully('user added'));
    } catch (err) {
        console.error(err);
        res.reply(messages.error('internal server'));
    }
};

controller.userSignin = async (req, res) => {
    try {
        const aUser = await Credential.findOne({
            sWalletAddress: req.body.sWalletAddress,
            sRole: 'user',
        });

        if (!aUser) {
            return res.reply(messages.not_found('user'));
        }

        const sRetrivedWalletAddress = sigUtil.recoverPersonalSignature({
            data: req.body.sMessage,
            sig: req.body.sSignature,
        });

        if (sRetrivedWalletAddress !== req.body.sWalletAddress) {
            return res.reply(messages.not_matched('user address'));
        }

        if (aUser.sAccountStatus === 'block') {
            return res.reply(
                messages.user_blocked('Sorry! your account hass been')
            );
        }

        if (aUser.sAccountStatus === 'block') {
            return res.reply(messages.user_blocked('your account has been'));
        }

        req.session['sUserId'] = aUser.sWalletAddress;
        req.session['sRole'] = aUser.sRole;

        console.warn(req.session);

        const sToken = jwt.sign(
            { user_id: aUser.sWalletAddress, sRole: aUser.sRole },
            process.env.TOKEN_KEY,
            {
                expiresIn: '2h',
            }
        );

        return res.reply(messages.successfully('signin'), {
            data: sToken,
        });
    } catch (err) {
        return res.reply({ message: messages.error('internal server') });
    }
};

controller.adminSignin = async (req, res) => {
    try {
        // TODO: Validate inputs
        const aMissing = [];

        if (!req.body.sEmail) {
            aMissing.push('Email');
        }

        if (!req.body.sPassword) {
            aMissing.push('Password');
        }

        if (aMissing.length > 0) {
            return res.reply(
                messages.require_field(
                    aMissing.length
                        ? aMissing.join(' ,') + 'are'
                        : aMissing + 'is'
                )
            );
        }

        if (_.isEmail(req.body.sEmail)) {
            return res.reply(
                messages.custom({
                    code: 400,
                    message: 'Enter a proper emalid ',
                })
            );
        }

        const aAdmin = await Credential.findOne({
            sEmail: req.body.sEmail,
            sRole: 'admin',
        });

        if (!aAdmin) {
            return res.reply(messages.not_found('user'));
        }

        const password = await bcrypt.compare(
            req.body.sPassword,
            aAdmin.sPassword
        );

        // Try to optimize
        if (password != true) {
            return res.reply(messages.invalid('password is'));
        }

        // Give relevant name
        req.session['sUserId'] = aAdmin.sEmail;
        req.session['sRole'] = aAdmin.sRole;

        log.warn(req.session);

        const sToken = jwt.sign(
            { user_id: aAdmin.sEmail, sRole: aAdmin.sRole },
            process.env.TOKEN_KEY,
            {
                expiresIn: '2h',
            }
        );

        return res.reply(messages.successfully('admin verified'), {
            data: sToken,
            sRole: aAdmin.sRole,
        });
    } catch (err) {
        log.red(err);
        return res.reply(messages.error('intrnal server'));
    }
};

controller.logOut = (req, res) => {
    try {
        req.session.destroy();
        res.clearCookie();
        res.reply(messages.successfully('logout'));
    } catch {
        res.reply(messages.error('internal server'));
    }
};

module.exports = controller;
