pragma solidity ^0.8.0;

contract learnStruct {

    struct Movies{
        string title;
        string director;
        uint id;
    }

    Movies comedy;

    function setMovie() public {
        comedy = Movies("Chup chup ke", "Priyadarshan", 19854);
    }

    function getMovie() public view returns(string memory, string memory, uint){
        return (comedy.title, comedy.director, comedy.id);
    }
}