pragma solidity >=0.7.0 <0.9.0;

contract learnEnum{

    enum shirtColor {RED, WHITE, GREEN , BLUE}
    shirtColor choice;
    shirtColor constant defaultChoice = shirtColor.BLUE;

    function setWhite() public {
        choice = shirtColor.WHITE;
    }

    function setRed() public {
        choice = shirtColor.RED;
    }

    function setGreen() public {
        choice = shirtColor.GREEN;
    }

    function getChoice() public view returns(shirtColor){
        return choice;
    }

    function getDefaultChoice() public view returns(uint){
        return uint(defaultChoice);
    }
}
