//SPDX-License-Identifier: Rahul pithadia

pragma solidity ^0.8.0;

contract ERC721{
    string private name;
    string private symbol;

    mapping(uint => address) private owners;
    mapping(address => uint) private balances;
    mapping(uint => address) private tokenApprovals;
    mapping(address => mapping(address => bool)) private operatorApprovals;

    event Transfer(address indexed _from, address indexed _to, uint indexed _tokenId);
    event Approval(address indexed _owner, address indexed _approved, uint indexed _tokenId);
    event ApprovalForAll(address indexed _owner, address indexed _operator, bool _approved);

    constructor(string memory _name, string memory _symbol ){
        name = _name;
        symbol = _symbol;
    }
    
     function exists(uint256 tokenId) internal view returns (bool) {
        return owners[tokenId] != address(0);
    }

    function mint (address to, uint tokenId) public {
        require(to != address(0), " mint to the zero address");
        require(owners[tokenId] == address(0) , " token already minted");
        balances[to]+=1;
        owners[tokenId] = to;
        emit Transfer(address(0), to, tokenId);
    }

    function approve(address _approved, uint256 _tokenId) public {
        tokenApprovals[_tokenId] = _approved;
        emit Approval(ownerOf(_tokenId), _approved, _tokenId);
    }

    function setAppovalForAll(address _operator, bool _approved) public{
        require(_operator != msg.sender, "operatoe should not be owner");
        operatorApprovals[msg.sender][_operator] = _approved;
        emit ApprovalForAll(msg.sender, _operator, _approved);
    }

    function getapproved(uint tokenId)public view returns (address){
        require(tokenApprovals[tokenId] != address(0), "token id not exist");
       return tokenApprovals[tokenId];
    }
    function isApprovedForAll(address owner, address operator) public view returns(bool){
        return operatorApprovals[owner][operator];
    }
    
    function isOperatorOrOwner(address operator, uint256 tokenId) public  view returns(bool succuss) {
        require(owners[tokenId] != address(0), "ERC721: operator query for nonexistent token");
        address owner = ownerOf(tokenId);
        if(operator == owner || operatorApprovals[owner][operator] || getapproved(tokenId) == operator)
        return true;
    }

    function balanceOf(address owner) external view returns (uint){
        return balances[owner];
    }
     
    function ownerOf(uint tokenId) public view returns(address){
        address owner = owners[tokenId];
        require(owner != address(0), "invalide token Id");
        return owner;
    }

    function transferfrom(address from, address to, uint tokenId) public{
        require(ownerOf(tokenId) == from, "transfer from incorrect address");
        require(owners[tokenId] != address(0), "token id not exist");
        require(isOperatorOrOwner(from, tokenId), "sender is not owner not oprator");
        balances[from]-= 1;
        balances[to] += 1;
        owners[tokenId] = to;

        emit Transfer(from, to , tokenId);
    }
}