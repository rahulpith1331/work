pragma solidity ^0.8.0;

contract donation {
 
    address public sender;
    address  public receiver ; 
    mapping (address => uint) public balances;
    
    constructor() public{
        sender = msg.sender;
    }
    
    modifier Sender{
        require(msg.sender == sender, "sorry you are not a sender.");
        _;
    }
    
    event Sent(address from, address to, uint amount);

    
    function depositFundIntoWallet() public payable  {
        require(msg.value != 0, "You need to deposit some amount of funds into wallet!");
        balances[msg.sender] += msg.value;
    }
   
    function donatetoreceiver(address _receiver, uint amount) public Sender {
        require(amount <= balances[msg.sender], "You have insuffient funds");
        balances[msg.sender] -= amount;
        balances[_receiver] += amount;
       receiver = _receiver;
        emit Sent(msg.sender, _receiver, amount);
    } 
}