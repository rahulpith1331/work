const prompt = require("prompt-sync")();

let square = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] 

function main() {
    let player = 1, i, choice;
    let mark = "";
    console.log("\n\n\tTic Tac Toe\n");
    let player1 = prompt("Player 1 please enter your name -> ")
    let player2 = prompt("Player 2 please enter your name -> ")

    do {

        board()
        //assigning player change
        player = (player % 2 === 1) ? 1 : 2;
       // Assigning name of player as per there numbers
        (player == 1) ? console.log("Player ->" + player1) : console.log("Player ->" + player2)
        choice = prompt("enter your move in number from matrix -> ")
        // Assigning mark as X or O as per Chance of a player
        mark = (player == 1) ? "X" : "O";
        // this condition will check the user input and if 
        //input is correct or not, and if it is correct then 
        //it will replace number to mark

        if (choice == 1 && square[1] == '1')
            square[1] = mark;

        else if (choice == 2 && square[2] == '2')
            square[2] = mark;


        else if (choice == 3 && square[3] == '3')
            square[3] = mark;

        else if (choice == 4 && square[4] == '4')
            square[4] = mark;

        else if (choice == 5 && square[5] == '5')
            square[5] = mark;

        else if (choice == 6 && square[6] == '6')
            square[6] = mark;

        else if (choice == 7 && square[7] == '7')
            square[7] = mark;

        else if (choice == 8 && square[8] == '8')
            square[8] = mark;

        else if (choice == 9 && square[9] == '9')
            square[9] = mark;

        else {
            console.log("invalide move");
            player--;
            //choice.trim().get()
        }
        i = checkwin()
        player++
        // this will clear board after every chance of player.
        console.clear()
        /* here  1 means  winner
                -1 means game is running
                 0 means game has draw */
    } while (i == -1);
    board();

    if (i == 1)
        (mark === 'X') ? console.log("\n\nPlayer " + player1 + " win") : console.log("\n\nPlayer " + player2 + " win")
    else
        console.log("\n\nGame draw");
}
 // This function print board of Tic-Tac-Toe.
function board() {
    console.log("\nPlayer (X)  -  Player (O)\n")
    console.log("     |     |     " + "\n"
        + "  " + square[1] + "  |  " + square[2] + "  |  " + square[3] + "\n"
        + "_____|_____|_____" + "\n"
        + "     |     |     " + "\n"
        + "  " + square[4] + "  |  " + square[5] + "  |  " + square[6] + "\n"
        + "_____|_____|_____" + "\n"
        + "     |     |     " + "\n"
        + "  " + square[7] + "  |  " + square[8] + "  |  " + square[9] + "\n"
        + "     |     |     ")
}

function checkwin() {
    // Checking winning possbility such as vertical, horizontal and diagonal
    if ((square[1] === square[2] && square[2] === square[3]) ||
        (square[4] === square[5] && square[5] === square[6]) ||
        (square[7] === square[8] && square[8] === square[9]) ||
        (square[1] === square[5] && square[5] === square[9]) ||
        (square[1] === square[4] && square[4] === square[7]) ||
        (square[3] === square[5] && square[5] === square[7]) ||
        (square[2] === square[5] && square[5] === square[8]) ||
        (square[3] === square[6] && square[6] === square[9]))
        return 1;
        // Checking a input position from range 1 to 9.
    else if (square[1] != '1' && square[2] != '2' && square[3] != '3' && square[4] != '4' && square[5] != '5' && square[6] != '6' && square[7] != '7' && square[8] != '8' && square[9] != '9')
        return 0
    else
        return -1
}
main()

