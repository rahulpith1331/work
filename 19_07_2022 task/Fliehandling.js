const fs = require("fs");

//1) Opening File 
fs.open("sample.txt", "w", (err, file) => {
   if (err) throw err;
   console.log(file);
});

//2) Writing into File
fs.writeFile("sample.txt", "Hello! I'm a new member of the Raw Team. my name is Rahul", (err)=>{
    if (err) throw err;
    console.log("Completed!!")
})

//3) Read file
fs.readFile("sample.txt", (err,data)=>{
    if (err) throw err;
    console.log(data.toString())
})

//4) Appen file
fs.appendFile("sample.txt"," sample1.txt", (err)=>{
    if(err)throw err;
    console.log("Completed!!")
})