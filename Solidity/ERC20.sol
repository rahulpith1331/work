//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./IERC20.sol";

contract ERC20 is IERC20{

    string private Name;
    string private Symbol;
    uint256 private Decimals = 18;
    uint256 private totalsupply;
    uint256 private mintedcoin;
    address private msgsender = msg.sender;

    mapping (address => uint) private balances;
    mapping(address => mapping(address => uint256)) private allowances;

      
    constructor(string memory name_, string memory symbol_, uint _totalsupply) {
        Name = name_;
        Symbol = symbol_;
       totalsupply= _totalsupply;
    }
  
    
    function mint(address account, uint256 amount) public virtual {
        require(account != address(0), "ERC20: mint to the zero address");
       require(amount+mintedcoin<= totalsupply,"ERC20: Reached totalsupply");

       // _beforeTokenTransfer(address(0), account, amount);
       mintedcoin+=amount;
        balances[account] += amount;
        emit Transfer(address(0), account, amount);

       // _afterTokenTransfer(address(0), account, amount);
    }

  function name() public view returns(string memory){
       return Name;
    }

    function symbol()public view returns(string memory){
        return Symbol;
    }

   function decimals()public view returns(uint){
        return Decimals;
    }


    function balanceOf(address owner) public view override returns(uint){
        return balances[owner];
    }

    function totalSupply() public view override returns(uint){
        return totalsupply;
    }

    function transfer(address to, uint amount) public override returns(bool success){
        require(balances[msg.sender] > amount, "Insufficient Balances");
        require(to != address(0), "address can't be Zero");
        balances[msgsender]-= amount;
        balances[to]+= amount;
        emit Transfer(msg.sender, to , amount);
        return true;
    }

    function approve(address spender, uint amount) public override returns(bool){
        require (spender != address(0)&& spender !=msgsender, "spender can't be owner");
        allowances[msg.sender][spender] = amount;
        emit Approval(msgsender, spender, amount);
        return true;
    }

    function allowance(address owner, address spender) public view override returns (uint){
        return allowances[owner][spender];
    }

     error insufficentAllowance(uint avalibleAllowance, uint transferAmount);

    function transferFrom(address from, address to, uint amount)public override  returns (bool sucess) {
        uint allowance = allowances[from][msg.sender];
        if(balances[from] < amount && allowance < amount)
        revert insufficentAllowance({avalibleAllowance: allowance, transferAmount: amount});
        balances[from]-=amount;
        balances[to]+=amount;
       allowances[from][msg.sender]-=amount;
        emit Transfer(from, to, amount);
        return true;
    }
}
