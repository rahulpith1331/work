const pinataSDK = require('@pinata/sdk');
const pinata = pinataSDK(' 2e9396f8ae6dcab93029', 'f73e7f0dc42688c89082690a3de8346670dbc8d2deb9a8c7747c8940d9694925');
const sourcePath = './metadata1';
const options = {
   pinataMetadata: {
        "name" : "Sprinkles Fisherton",
        "description" : "Friendly OpenSea Creature that enjoys long swims in the ocean.",
        "external_url" : "https://example.com/?token_id=1",
        "image" : "https://gateway.pinata.cloud/ipfs/QmaARiEyuJwoeaSJst63Qjynh6TA4HsX3yb3sEF4aJc2PL",
        "attributes" :[
        {
            "trait_type" : "level",
            "value" : 3
        },
        {
            "trait_type" : "stamina",
            "value" : 11.7
        },
        {
            "trait_type" : "personality",
            "value" : "sleepy"
        },
        {
            "display_type" : "boost_number",
            "trait_type" : "aqua_power",
            "value" : 30
        },
        {
            "display_type" : "boost_percentage",
            "trait_type" : "stamina_increase",
            "value" : 15
        },
        {
            "display_type" : "number",
            "trait_type" : "generation",
            "value" : 1
        }]
        },
   pinataOptions: {
       cidVersion: 0
   }
};
pinata.pinFromFS(sourcePath, options).then((result) => {
    //handle results here
    console.log(result);
}).catch((err) => {
    //handle error here
    console.log(err);
});