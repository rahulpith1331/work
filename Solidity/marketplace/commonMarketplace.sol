// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./ERC721minter.sol";
import "./ERC1155minter.sol";

contract MarketPlace is Context, Ownable{
       
       
    struct Sell{
      uint256 sellprice;
      uint256 quantity;
      
    }

    struct Auction{
      uint256 starttime;
      uint256 endtime;
      uint256 bid;
      uint256 quantity;
      bool started;
      bool ended;
    }

    struct Bid{
      address bider;
      uint256 bidamount;
      uint256 quantity;
    }

    //Private Variable

    Sell private _sell;
    ERC721Minter private _erc721minter;
    ERC1155Minter private _erc1155minter;
    uint56 private _tokenid;
    uint256 lastmomentbid = 10 minutes;
    string private _name;
    string private _symbol;
    string private _uri;
    address private _admin;
    enum  _tokentypes {ERC721, ERC1155}


    //Mapping 

    mapping (address =>uint256) private _balances;
    mapping (uint256 => bool)private tokentype; 
    mapping (uint256 => Sell) private _tokenselldetails;
    mapping (uint256 => uint256) private _tokenmintlist;
    mapping (uint256 => address)private _owners;
    mapping (uint256 => Auction) private _auctionlist;
    mapping (uint256  => Bid) private _higherbids;
    mapping (uint256  => Bid) private _lowerbids;
    mapping (uint256 => mapping (uint256 => address)) private _claimedtoken;

    //Events

    event Selllist (address indexed owner, uint256 indexed tokenid, uint256 quantity, uint256 priceperquantity);
    event Tokensold ( address indexed to, uint256 indexed tokenid, uint256 quantity, uint256 totalprice);
    event Auctionlist (address indexed from, uint256 indexed tokenid, uint256 quantity, uint256 intialbidamount, uint256 starttime, uint256 endtime);
    event Biding(address indexed from, uint256 indexed tokenid, uint256 quantity, uint256 bidamount, uint256 bidtime);
    event Auctionsettlement(address indexed from , address indexed to, uint256 bidprice, uint256 tokenid, uint quantity);

    // Constructor
    constructor(address  erc1155minter, address erc721minter) {
          _admin = msg.sender;
          _erc721minter = ERC721Minter(erc721minter);
          _erc1155minter = ERC1155Minter(erc1155minter);
    }

    function mintToken(bool mintchoice, uint56 _quantity) public onlyOwner{
     address msgsender = msg.sender;
       _tokenid++;
       _owners[_tokenid] = msgsender;
       _tokenmintlist[_tokenid] = _quantity;
     if(mintchoice == true){
       require(_quantity == 1, "MarketPlace: Select ERC1155 to mint multiple copy of single token id");
       _erc721minter.mint(msgsender, _tokenid);
     }else {
      tokentype[_tokenid]= mintchoice;
       _erc1155minter.mint(msgsender, _tokenid, _quantity, "");
     }
   }

    function sellToken(bool tokentypes, uint56 tokenid, uint256 priceoftoken, uint56 _quantity) public onlyOwner{
      
      require(_exist(tokenid), "Market Place: Token id is not minted yet.");
      require(priceoftoken >0, "Market Place: Price shouldn't be zero or less");
      uint256 remainingquantity= _tokenmintlist[tokenid]-_tokenselldetails[tokenid].quantity - _auctionlist[tokenid].quantity;
        uint256 quantity = _auctionlist[tokenid].quantity+_quantity;
      if(tokentypes == true){  
        require(_auctionlist[tokenid].quantity==0,"already listed on auction");
        require(_quantity == 1, "MarketPlace: Select ERC1155 to mint multiple copy of single token id");
        _tokenselldetails[tokenid] = Sell(priceoftoken,_quantity);       

      } else{
       require(remainingquantity >= quantity, "Market Place: Insufficent quantity for sell");
        _tokenselldetails[_tokenid].sellprice = priceoftoken;
        _tokenselldetails[_tokenid].quantity+= _quantity;
        }
         emit Selllist(msg.sender, tokenid, _quantity, priceoftoken);
    }

    function buytoken( bool tokentypes ,uint56 tokenid, uint56 quantity) public payable{
      require(msg.sender != _owners[tokenid], "MarketPlace: Owner can't buy token ");
      require(_tokenselldetails[tokenid].sellprice != 0,"Market Place: Token not out for sell yet.");
      uint256 totalprice = _tokenselldetails[tokenid].sellprice * quantity;
      require(msg.value == totalprice,"Market Place: Not entered proper quantity.");
      address msgsender = msg.sender;
      if(tokentypes == true){
        require(quantity == 1,"MarketPlace: Enter proper quantity to buy");
          payable(_owners[tokenid]).transfer(msg.value);
          _erc721minter.safeTransferFrom(_owners[tokenid], msgsender, tokenid, "");

      }else{
        require(quantity <= _tokenselldetails[tokenid].quantity,"Market Place: Token quantity insufficent");
        payable(_owners[tokenid]).transfer(msg.value);
        _erc1155minter.safeTransferFrom(_owners[tokenid], msgsender, tokenid, quantity, "");
      }
      emit Tokensold ( msg.sender, tokenid, quantity, totalprice);
     }

    function auctionToken(bool tokentypes ,uint56 tokenid, uint56 _quantity, uint56 initialbidamount) public onlyOwner{
      require(_auctionlist[tokenid].started == false,"MarketPlace: Auction for this token has started");
      uint256 remainingquantity= _tokenmintlist[tokenid]-_tokenselldetails[tokenid].quantity - _auctionlist[tokenid].quantity;
      uint256 quantity = _auctionlist[tokenid].quantity+_quantity;
      uint256 _starttime = block.timestamp;
      uint256 _endtime = _starttime + 2 minutes;

      if(tokentypes == true){
        require(_tokenselldetails[tokenid].quantity == 0, "Market Place: Already listed on sell");
        require(_quantity == 1, "MarketPlace: Select ERC1155 to auction more than one quantity");
        _auctionlist[tokenid] = Auction(_starttime, _endtime, initialbidamount, _quantity, true, false);
      }else {
      require(remainingquantity >= quantity, "marketplace: Insufficent quantity for auction");
         _auctionlist[tokenid] = Auction(_starttime, _endtime, initialbidamount, _quantity, true, false);
      }

      if(block.timestamp ==_auctionlist[tokenid].endtime){
        _auctionlist[tokenid].started = false;
        _auctionlist[tokenid].ended = true;
      }
      
      emit Auctionlist(msg.sender, tokenid, _quantity,  initialbidamount, _starttime, _endtime);
    }

    function bid(bool tokentypes, uint256 tokenid) public payable  {
      require(msg.sender != _owners[tokenid], "MarketPlace: Owner can't bid token ");
      require (block.timestamp <_auctionlist[tokenid].endtime, "Market Place: Sorry auction is ended");
      require(msg.value > _auctionlist[tokenid].bid ,"Market  Place: Bid should be higher than initial bid");
      uint256 bidamount = msg.value;
          
      if(tokentypes == true){
          if(_higherbids[tokenid].bidamount > 0  || _higherbids[tokenid].bidamount  < msg.value){
          _lowerbids[tokenid] = _higherbids[tokenid];
          _higherbids[tokenid]= Bid(msg.sender, bidamount, _auctionlist[tokenid].quantity);
          }
          payable(_lowerbids[tokenid].bider).transfer(_lowerbids[tokenid].bidamount);
      }else{
         if(_higherbids[tokenid].bidamount > 0  || _higherbids[tokenid].bidamount  < msg.value){
           _lowerbids[tokenid] = _higherbids[tokenid];
          _higherbids[tokenid]= Bid(msg.sender, bidamount, _auctionlist[tokenid].quantity);
         }
           payable(_lowerbids[tokenid].bider).transfer(_lowerbids[tokenid].bidamount); 
      }

        uint256 lasttensecs = 10 seconds;
        if(_auctionlist[tokenid].endtime - block.timestamp == lasttensecs ){
          _auctionlist[tokenid].endtime += 15 minutes;
        }
        emit Biding(msg.sender, tokenid, _auctionlist[tokenid].quantity, bidamount, block.timestamp);
    }

    function settleAuction(bool tokentypes, uint256 tokenid) public {
      require(block.timestamp >= _auctionlist[tokenid].endtime, "Market Place: Auction isn't ended yet");
      require(_higherbids[tokenid].bider == msg.sender, "Market Place: you are not a highest bider");
      if(tokentypes == true){
        _erc721minter.safeTransferFrom(_owners[tokenid], msg.sender, tokenid, " ");
        payable(_owners[tokenid]).transfer(address(this).balance);
      }else{
        _erc1155minter.safeTransferFrom(_owners[tokenid], msg.sender, tokenid, _auctionlist[tokenid].quantity, "");
        payable(_owners[tokenid]).transfer(address(this).balance);
      }
      emit Auctionsettlement(_owners[tokenid], msg.sender, address(this).balance, tokenid, _auctionlist[tokenid].quantity);
    }

    function balanceOf(address _user, _tokentypes tokentypes, uint256 tokenid ) public view returns(uint256 balances){
      require (_user != address(0), "Market Place: Zero addresses are not allowed.");
      require(_exist(_tokenid), "Market Place: Token id is not minted yet.");
      if(tokentypes == _tokentypes.ERC721){
        balances = _erc721minter.balanceOf(_user);
      }else if(tokentypes==_tokentypes.ERC1155){
        balances = _erc1155minter.balanceOf(_user, tokenid);
      }
    }

    function tokenType(uint256 tokenid) public view returns (bool){
      return tokentype[tokenid];
    }

    function _exist(uint256 tokenid)internal view virtual returns (bool) {
        return _owners[tokenid] != address(0);
    }

    function checkPriceOfToken(uint256 tokenid) public view returns(uint256){
        require(_tokenselldetails[tokenid].sellprice != 0 ,"Market Place: Token not out for sell yet.");
        return _tokenselldetails[tokenid].sellprice;
    }

    function checkQuantityOnSellOfToken(uint256 tokenid) public view returns(uint256){
       require(_tokenselldetails[tokenid].quantity !=  0,"Market Place: Token not out for sell yet.");
       return _tokenselldetails[tokenid].quantity;
    }

    function checkhighestbider(uint256 tokenid) public view returns(Bid memory){
      return _higherbids[tokenid];
    }

    function checklowestestbider(uint256 tokenid) public view returns(Bid memory){
      return _lowerbids[tokenid];
    }

}