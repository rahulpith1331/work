//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract MutiSignatureWallet is  Ownable { 
    struct RequestInfo{
        uint requestamount;
        address requestsender;
        requeststatus requeststate;
    }
  
    IERC20 private _token;
    uint public _totalsupply;
    uint  private _requestid;
    address private _admin;
    enum  accounttype {MAKER, CHECKER}
    enum requeststatus {PENDING, ACCEPTED, REJECTED}

    mapping (address => uint256) private _balances;
    mapping (address => accounttype) private account;
    mapping (address => mapping(address=>uint)) private  _allowances;
    mapping (uint256 => RequestInfo) private _tokenrequest;

    event Transfer(address indexed from, address indexed to, uint amount);

    
        // function carries transaction
      constructor (address token) {
        _token = IERC20(token);
    }

    function setMaker(address maker) public onlyOwner{
        account[maker] = accounttype.MAKER;
    }

     function setChecker(address _checker) public onlyOwner{
        account[_checker] = accounttype.CHECKER;
    }

    function requestToken(uint _amount) public {
        require(account[_msgSender()] == accounttype.MAKER|| account[_msgSender()] == accounttype.CHECKER,"Token request are only done by apporved maker or Checker");
       _requestid++;
       _tokenrequest[_requestid] = RequestInfo(_amount, _msgSender(),requeststatus.PENDING);  
    }

   function acceptRequest(uint _index, bool _pool) public {
        require(_msgSender()!= _tokenrequest[_index].requestsender && _msgSender() != owner(), "Only checker can approve or Reject pending request");
        require(account[_msgSender()]== accounttype.CHECKER,"Token request will only be apporve or reject by checker");
        require(_tokenrequest[_index].requestamount <= _token.balanceOf(address(this)), "Contract balance is insufficent");
        if(_pool == true){
            _tokenrequest[_index].requeststate = requeststatus.ACCEPTED;
            _token.transfer( _tokenrequest[_index].requestsender, _tokenrequest[_index].requestamount);
        }else if (_pool == false){
            _tokenrequest[_index].requeststate = requeststatus.REJECTED;
        }

    }

    //  view/pure function only

    function getAccountType( address _account) public view returns(accounttype){
        return account[_account];
    }

    function getRequestStatues(uint _index) public view returns(RequestInfo memory){
     return _tokenrequest[_index];
    }
 
}