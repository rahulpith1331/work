const { Credential } = require('../../../models/index');

const controller = {};

controller.getUserData = async (req, res) => {
    try {
        const oSortingOrder = {};

        oSortingOrder[
            req.body.columns[parseInt(req.body.order[0].column)].data
        ] = req.body.order[0].dir === 'asc' ? 1 : -1;

        let searchStr;

        if (!req.body.search.value) {
            searchStr = {
                sRole: {
                    $ne: 'admin',
                },
            };
        } else {
            const regex = new RegExp(req.body.search.value, 'i');

            searchStr = {
                sRole: {
                    $ne: 'admin',
                },

                sUsername: {
                    $regex: regex,
                },
            };
        }

        const nTotalRecords = await Credential.countDocuments({
            sRole: {
                $ne: 'admin',
            },
        });

        const nFilterRecords = await Credential.countDocuments(searchStr);
        const aUser = await Credential.aggregate([
            {
                $match: { sRole: 'user' },
            },
            {
                $match: searchStr,
            },
            {
                $sort: oSortingOrder,
            },
            {
                $skip: Number(req.body.start),
            },
            {
                $limit: Number(req.body.length),
            },
        ]);

        return res.reply(messages.successfully('users found'), {
            data: aUser,
            draw: req.body.draw,
            recordsTotal: nTotalRecords,
            recordsFiltered: nFilterRecords,
        });
    } catch (err) {
        return res.reply(messages.error('internal server'));
    }
};

controller.setAccountStatus = async (req, res) => {
    try {
        // Validate wallet address
        const oUser = await Credential.find({
            sWalletAddress: req.body.sWalletAddress,
            sRole: 'user',
        });

        if (!oUser) {
            return res.reply(messages.not_found('user'));
        }
        if (
            req.body.sAccountStatus != 'unblock' &&
            req.body.sAccountStatus != 'block'
        ) {
            return res.reply(messages.invalid_req());
        }

        // Check if the wallet address exist or not

        await Credential.findOneAndUpdate(
            { sWalletAddress: req.body.sWalletAddress, sRole: 'user' },
            { sAccountStatus: req.body.sAccountStatus }
        );

        return res.reply(messages.updated('user status'));
    } catch (err) {
        log.red(err);
        return res.reply(messages.error('internal server'));
    }
};

module.exports = controller;
