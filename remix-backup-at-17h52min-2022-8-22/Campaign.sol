//"SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

contract CampaignFactory{
    address [] public deployedCampaigns;
    function createCampaign(uint _minimum) public {
       address newCampaign = address(new Campaign(_minimum, msg.sender));
        deployedCampaigns.push(newCampaign);
    }

    function getDeployedCampaigns() public view returns(address[] memory ){
        return deployedCampaigns;
    }
}

 contract Campaign {

    struct Request{
         string descripition;
         uint value;
         address recipient;
         bool completed;
         uint approvalsCount;
         mapping (address => bool) approvals;
    }
     
     address public manager;
     uint public minimumContribution;
     address [] public approver;
     Request [] public requests;
     uint public approvalscount;
     mapping (address => bool) approvers;

    constructor (uint _minimum , address _creator) {
         manager = _creator;
         minimumContribution = _minimum;
    }

    function contribute() public payable{
         require(msg.value >= minimumContribution);
        approvers[msg.sender] = true;
        approvalscount++;
     }

     modifier restricted(){
          require(msg.sender == manager);
          _;
     }
      uint numRequests;
    mapping (uint => Request) request;

     function createRequest(string memory  _descripition, uint _value, address _recipient ) public restricted{
        
       Request storage r = request[numRequests++];
           r.descripition= _descripition;
           r.value=_value;
           r.recipient=_recipient;
           r.completed= false;
           r.approvalsCount= 0;
       
       
       //requests.push(newRequest);
    }

     function approveRequest(uint index) public {
         Request storage req = requests[index];

         require(approvers[msg.sender]);
         require(!req.approvals[msg.sender]);

         req.approvals[msg.sender] = true;
        req.approvalsCount++;
    }

    function finalizeRequest(uint index) public restricted{
        Request storage req = requests[index];
        require(req.approvalsCount > (approvalscount/2));
        require(!req.completed);
        payable(req.recipient).transfer(req.value);
        req.completed = true;
    }

    function getRequestCount() public view returns(uint){
        return requests.length;
    }

 }