const controller = {};

controller.userpage = async (req, res) => {
    res.render('userview/User');
};

controller.userProfile = async (req, res) => {
    res.render('userview/Userprofile');
};

module.exports = controller;
