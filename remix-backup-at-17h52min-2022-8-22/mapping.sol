pragma solidity ^0.8.0;

contract learnMapping{

    struct Developer{
        string name;
        string department;
        uint empid;
    }

    mapping (address => Developer)  developers;
    address [] details;
   

    function setDeveloper(address _address,  string memory _name, string memory _department, uint _empid) public{
        Developer storage developer = developers[_address];

        developer.name = _name;
        developer.department = _department;
        developer.empid = _empid;

        details.push(_address);

    }
     function getInstructor(address _address) public view  returns (string memory, string memory, uint) {
        return (developers[_address].name, developers[_address].department, developers[_address].empid);
    }

    function getdetails() public view returns(address[] memory){
        return details;
    }

    
}