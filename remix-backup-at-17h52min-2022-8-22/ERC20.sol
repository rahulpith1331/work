//SPDX-License-Identifier: Rahul pithdia
pragma solidity ^0.8.0;

contract ERC20{
    string private name;
    string private symbol;
    uint private totalSupply;

    mapping (address => uint) private balances;
    mapping (address  => mapping (address => uint)) private allowed;
    
    event Approval(address indexed _owner, address indexed _spender, uint256 _amount);
    event Transfer(address indexed _from, address indexed _to, uint256 _amount);

    constructor (uint _initialAmount, string memory _tokenName, string memory _tokenSymbol, uint _totalSupplyofToken){
        balances[msg.sender] = _initialAmount;
        name = _tokenName;
        symbol = _tokenSymbol;
        totalSupply = _totalSupplyofToken;
    }

    function tokenName() external view  returns (string memory) {
        return name;
    }

    function tokenSymbol() external view  returns (string memory){
        return symbol;
    }

    function totaltokenSupply() external view  returns(uint){
        return totalSupply;
    }

    function balanceOf(address _owner) external view returns (uint256) {
        return balances[_owner];
    }

    error insufficentBalance(uint avalibleBalance, uint requestedAmount);

    function transfer(address to, uint amount) external returns(bool success){
      if(balances[msg.sender] < amount)
      revert insufficentBalance({avalibleBalance: balances[msg.sender], requestedAmount: amount });
      balances[msg.sender]-=amount;
      balances[to]+= amount;
      emit Transfer(msg.sender, to, amount);
      return true;
    }

     error insufficentAllowance(uint avalibleAllowance, uint transferAmount);

    function transferfrom(address from, address to, uint amount)external  returns (bool sucess) {
        uint allowances = allowed[from][msg.sender];
        if(balances[from] < amount && allowances < amount)
        revert insufficentAllowance({avalibleAllowance: allowances, transferAmount: amount});
        balances[from]-=amount;
        balances[to]+=amount;
        allowances-=amount;
        emit Transfer(from, to, amount);
        return true;
    }
    
    function approve(address _spender, uint256 _amount) external  returns (bool success){
        allowed[msg.sender][_spender] =_amount;
        emit Approval(msg.sender, _spender, _amount);
        return true;
    }

    function allowance(address _owner, address _spender) external view  returns (uint256 remaining){
        return allowed[_owner][_spender];
    }
}