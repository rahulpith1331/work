// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract ERC721Minter is ERC721, Ownable {
    constructor(string memory _name, string memory  _symbol) 
    ERC721( _name, _symbol) {
}

    function mint(address to, uint256 tokenId) external{
        require(tx.origin != msg.sender, "ERC721Minter: Only can mint this token." );
        _safeMint(to, tokenId);
    }

    function tokenTransferFrom(address from, address to, uint tokenid,bytes memory data) external  {
        _safeTransfer(from, to, tokenid, data);
    }
}