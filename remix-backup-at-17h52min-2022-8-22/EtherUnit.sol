pragma solidity ^0.8.0;

contract ehterUints{

    function test() public {

        assert(1000000000000000000 wei == 1 ether);
        assert(2000000000000000000 wei == 2 ether);

        assert(1e18  == 1 ether);
    }
}