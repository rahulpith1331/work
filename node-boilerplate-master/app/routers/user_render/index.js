const router = require('express').Router();
const controllers = require('./lib/controller');

router.get('/', controllers.userpage);
router.get('/userProfile', controllers.userProfile);

module.exports = router;
