const mongoose = require('mongoose');


const Credential = mongoose.Schema({
    sUsername: {
        type: String,
        require: [true, 'sUsername is missing'],
        validate: {
            validator: function () {
                return _.isUserName(this.sUsername);
            },
            message: (props) => `${props.value} is inappropriate Username`,
        },
    },

    sEmail: {
        type: String,
        require: [true, 'sEmail is missing'],
        lowercase: true,
        validate: {
            validator: function () {
                return !_.isEmail(this.sEmail);
            },
            message: (props) => `${props.value} is inappropriate Email`,
        },
    },

    sPassword: {
        type: String,
        require: [true, 'sPassword is missing'],
    },

    sWalletAddress: {
        type: String,
        require: [true, 'Wallet address is required'],
        unique: true,
        lowercase: true,
        validate: {
            validator: function () {
                return _.isWalletAddress(this.sWalletAddress);
            },
            message: (props) => `${props.value} is inappropriate password`,
        },
    },

    sRole: {
        type: String,
        default: 'user',
        enum: {
            values: ['user', 'admin'],
            message: '{VALUE} is not supported',
        },
        require: [true, 'sRole is missing'],
    },

    sAccountStatus: {
        type: String,
        default: 'unblock',
        enum: {
            values: ['block', 'unblock'],
            message: '{VALUE} is not supported',
        },
        require: [true, 'sAccountStatus is missing'],
    },
});

module.exports = mongoose.model('credential', Credential);
