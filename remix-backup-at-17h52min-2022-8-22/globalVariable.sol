//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract GlobalVariable{

    function getSenderAddress() public view returns (address){
        return msg.sender;
    }

    function getminersaddress() public view returns (address){
        return block.coinbase;
    }

    function getblocknumber() public view returns (uint){
        return block.number;
    }

    function getTimestamp () public view returns (uint){
        return block.timestamp;
    }

    function getmsgSignature() public view returns(bytes4){
        return msg.sig;
    }

    function getBlockDifficulty() public view returns (uint){
        return block.difficulty;
    }
}