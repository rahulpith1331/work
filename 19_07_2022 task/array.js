let array = [1,2,3,4,5,6,7,8,9,10,11,12,13];

function input(array) {
    return array.every((start, index, arr) =>
        arr[index - 1] + 1 === start ||
        start+ 1 === arr[1] ||
        index < 2 && arr[0] === 1 && arr[arr.length - 1] === 13
    );
}

console.log(input([1, 2, 3, 4, 5]));    
console.log(input([1, 2, 3]));  
console.log(input([8,9,10,11]));     
console.log(input([11,12,13]));
console.log(input([1,11,12,13]));  
console.log(input([1,8,9,10,11,12,13]))
console.log(input([1,4,5,6]))
console.log(input([1,3,4,5]))