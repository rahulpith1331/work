//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract MarketPlace{

    struct Item{
        string itemName;
        string itemDescription;
        address Creator;
        address buyer;
        uint price;
        bool sold;
    }
    mapping (address => uint) balances;
    mapping(uint => Item)  items;
    uint public itemCount;

     function depositFundIntoWallet() public payable  {
        require(msg.value != 0, "You need to deposit some amount of funds into wallet!");
        balances[msg.sender] += msg.value;
    }

    function addItem(string memory _name, string memory _description, uint _price) public  payable returns(uint){
        require(balances[msg.sender] >= 1200, "Places maintain minimum balace");
        itemCount++;
        items[itemCount].itemName = _name;
        items[itemCount].itemDescription = _description;
        items[itemCount].Creator = msg.sender;
        items[itemCount].price = _price;
        return itemCount;
    }

    function getItemDetail(uint index) public view returns(string memory Name, string memory Description, address Creator, uint price, bool sold){
         Item storage i = items[index];
         return (i.itemName, i.itemDescription, i.Creator, i.price, i.sold);
    }

    function removeItem(uint index) public returns(bool){
        Item storage i = items[index];
        require(i.Creator != address(0) && i.Creator== msg.sender, "Item not exist");
        require(!i.sold, "can't delete item it was sold");
        delete items[index];
        itemCount--;
        return true;
    }


    function buyItem(uint index) public payable{
        Item storage i = items[index];
        require(msg.sender != i.Creator, "buyer and creator should not be same");
        require(balances[msg.sender]>= i.price, "place deposit some amount");     
        balances[i.Creator]+= i.price;
        balances[msg.sender]-=i.price;
        i.buyer = msg.sender;
        i.sold = true;
    }

    function getBuyer(uint index) public view returns(address){
        Item storage i = items[index];
        return i.buyer;
    }

}
