const fs = require("fs");

const file1 = fs.open("sample.txt", "r", (err)=>{
    if (err) throw err;
    return
})

const file2 = fs.open("sample1.txt" , "r", (err)=>{
    if(err) throw err;
    return
})

const file3 = fs.open("sample3.txt", "w+", (err,file)=>{
    if (err) throw err;
    return file;
    
})



if(file1 === null|| file2 === null || file3 === null){
    console.log("couldn't open any file");
    return;
}

let fileData1
fs.readFile("sample.txt", (err,data)=>{
    if (err) throw err;
    fileData1=data.toString()

    fs.appendFile('sample3.txt', "Content of file 1 - > "+fileData1 + "\n", (err)=>{
        if (err ) throw err
        return
    })
})

let fileData2
fs.readFile("sample1.txt", (err,data)=>{
    if (err) throw err;
    fileData2=data.toString()

    fs.appendFile('sample3.txt', "Content of file 2 -> "+fileData2+"\n", (err)=>{
        if (err ) throw err
        return
    })
})



// operation on file

async function fileOperation(){
    const data = fs.readFileSync("sample3.txt", "utf-8")
    const len = data.length
    return len
}

async function uppercase(){
    const data = fs.readFileSync("sample3.txt","utf-8")
    const upper = data.toUpperCase()
    console.log(upper)
}


async function lowercase(){
    const data = fs.readFileSync("sample3.txt","utf-8")
    const lower = data.toLowerCase()
    console.log(lower)
}

fileOperation().then(res => console.log(res)).catch(err => {
    console.log('some error occured');
 })
uppercase().then(res => console.log(res)).catch(err => {
    console.log('some error occured');
})

lowercase().then(res => console.log(res)).catch(err => {
    console.log('some error occured');
})