const { MerkleTree } = require('merkletreejs');
const keccak256 = require('keccak256');
const util = require('ethereumjs-util');

// List of 5 public Ethereum addresses
const addresses = [
    '0x4B20993Bc481177ec7E8f571ceCaE8A9e22C02db',
    '0x5B38Da6a701c568545dCfcB03FcB875f56beddC4',
    '0xAb8483F64d9C6d1EcF9b849Ae677dD3315835cb2',
    '0x78731D3Ca6b7E34aC0F824c42a7cC18A495cabaB',
    '0x617F2E2fD72FD9D5503197092aC168c91465E7f2',
    '0x17F6AD8Ef982297579C203069C1DbfFE4348c372',
];

// Hash leaves
const leaves = addresses.map((addr) => keccak256(addr));


// Create tree
let merkleTree = new MerkleTree(leaves, keccak256, { sortPairs: true });
const buf2hex = (addr) => '0x' + addr.toString('hex');
console.log('Root:', buf2hex(merkleTree.getRoot()));
// console.log(merkleTree.toString());

// let address = addresses[0];
let address = '0x617F2E2fD72FD9D5503197092aC168c91465E7f2';
const leaf = keccak256(address);
const finalLeaf = util.bufferToHex(leaf);
let hashedAddress = keccak256(address);
let proof = merkleTree.getHexProof(hashedAddress);
console.log(finalLeaf);
console.log(proof);

// Check proof
// let v = merkleTree.verify(proof, hashedAddress, rootHash)
// console.log(v) // returns true
