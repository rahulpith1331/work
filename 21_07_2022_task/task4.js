function diamond(number) {
  str = "";
  for (i = 1; i < number + 1; i++) {
    str += "\n";
    for (j = 1; j <= number - i + 1; j++) {
      str += " ";
    }
    for (j = 1; j <= 2 * i - 1; j++) {
      if (j == 1 || j == 2 * i - 1) {
        str += "*";
      } else {
        str += " ";
      }
    }
    for (j = 1; j <= number - i + 1; j++) {
      str += " ";
    }
  }
  for (i = number - 1; i > 0; i--) {
    str += "\n";
    for (j = 1; j <= number - i + 1; j++) {
      str += " ";
    }
    for (j = 1; j <= 2 * i - 1; j++) {
      if (j == 1 || j == 2 * i - 1) {
        str += "*";
      } else {
        str += " ";
      }
    }
    for (j = 1; j <= number - i + 1; j++) {
      str += " ";
    }
  }
  return str;
}
console.log(diamond(5));
