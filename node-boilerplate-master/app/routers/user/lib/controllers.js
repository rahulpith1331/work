const { Credential } = require('../../../models/index');
const sigUtil = require('eth-sig-util');
const controller = {};

controller.editProfile = async (req, res) => {
    try {
        // Validate data
        const aUser = await Credential.findOne({
            sWalletAddress: req.session.sUserId,
        });

        const oUserData = new Credential(aUser);
        const oError = oUserData.validateSync();

        if (oError) {
            return res.status(406).send({ message: _.errorMessage(oError) });
        }

        const sRetrivedWalletAddress = sigUtil.recoverTypedSignature({
            data: req.body.sData,
            sig: req.body.sSignature,
        });

        log.warn(sRetrivedWalletAddress);

        if (sRetrivedWalletAddress !== aUser.sWalletAddress) {
            return res.reply({ message: messages.wrong_credentials() });
        }

        const oUser = {
            sUsername: req.body.sUsername,
            sEmail: req.body.sEmail,
        };

        await Credential.findOneAndUpdate(
            { sWalletAddress: req.session.sUserId },
            oUser
        );
        return res.reply(messages.updated('profile'));
    } catch (err) {
        log.red(err);
        return res.reply(messages.error('internal server'));
    }
};

controller.getUserData = async (req, res) => {
    try {
        console.warn(req.session);
        const aUser = await Credential.aggregate([
            {
                $match: {
                    sWalletAddress: req.session.sUserId,
                    sRole: req.session.sRole,
                },
            },
            { $project: { _id: 0, sPassword: 0, __v: 0 } },
        ]);
        if (!aUser.length) {
            return res.reply(messages.not_found('user'));
        }
        if (aUser.sAccountStatus === 'block') {
            return res.reply({
                messages: messages.user_blocked('your account has been'),
            });
        }
        return res.reply(messages.successfully('user found'), {
            data: aUser,
        });
    } catch (err) {
        console.warn(err);
        return res.reply(messages.error('internal server'));
    }
};

controller.checkUserExist = async (req, res) => {
    try {
        const aUser = await Credential.findOne({
            sWalletAddress: req.body.sWalletAddress,
            sRole: 'user',
        });

        log.warn(aUser);

        return res.reply(
            aUser
                ? messages.successfully('user found')
                : messages.not_found('user')
        );
    } catch (err) {
        console.warn(err);
        return res.reply(messages.error('internal server'));
    }
};

module.exports = controller;
