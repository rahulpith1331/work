//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "./IERC721.sol";

contract ERC721 is IERC721{

    string private name_;
    string private symbol_;
    uint private decimals_;

    mapping (address =>uint) private balances;
    mapping(uint => address) private owners;
    mapping(uint => address) private tokenApprovals;
    mapping(address => mapping(address => bool)) private operatorApprovals;

    constructor(string memory _name, string memory _symbol , uint _decimals){
        name_=_name;
        symbol_=_symbol;
        decimals_=_decimals;
    }

    function name() public override view returns(string memory){
        return name_;
    }

    function symbol()public override view returns(string memory){
        return symbol_;
    }

    function decimals() public view returns(uint){
        return decimals_;
    }

    function balanceOf(address _owner) public override view returns (uint256){
        return balances[_owner];
    }

    function mint (address to, uint tokenId) public {
        require(to != address(0), " mint to the zero address");
        require(!exists(tokenId) , " token already minted");
        balances[to]+=1;
        owners[tokenId] = to;
        emit Transfer(address(0), to, tokenId);
    }

    function ownerOf(uint256 _tokenId) public override view returns (address){
        return owners[_tokenId];
    }


    function approve(address _approved, uint256 _tokenId) public override{
        tokenApprovals[_tokenId] = _approved;
        emit Approval(ownerOf(_tokenId), _approved, _tokenId);
    }

    function transferFrom(address _from, address _to, uint256 _tokenId) external override{
        require(ownerOf(_tokenId) == _from, "transfer from incorrect address");
        require(owners[_tokenId] != address(0), "token id not exist");
        require(isOperatorOrOwner(_from, _tokenId), "sender is not owner not oprator");
        balances[_from]-= 1;
        balances[_to] += 1;
        owners[_tokenId] = _to;
        emit Transfer(_from, _to , _tokenId);
    }

    function setApprovalForAll(address _operator, bool _approved) public override {
        require(_operator != msg.sender, "operatoe should not be owner");
        operatorApprovals[msg.sender][_operator] = _approved;
        emit ApprovalForAll(msg.sender, _operator, _approved);
    }

    function getApproved(uint256 _tokenId) public override view returns (address){
        require(tokenApprovals[_tokenId] != address(0), "token id not exist");
       return tokenApprovals[_tokenId];
    }
    function isApprovedForAll(address _owner, address _operator) public override view returns (bool){
        return operatorApprovals[_owner][_operator];
    }
    
    function isOperatorOrOwner(address _operator, uint256 _tokenId) public  view returns(bool succuss) {
        require(owners[_tokenId] != address(0), "ERC721: operator query for nonexistent token");
        address owner = ownerOf(_tokenId);
        if(_operator == owner || operatorApprovals[owner][_operator] || getApproved(_tokenId) == _operator)
        return true;
    }

     function exists(uint256 tokenId) internal view returns (bool) {
        return owners[tokenId] != address(0);
    }

}

