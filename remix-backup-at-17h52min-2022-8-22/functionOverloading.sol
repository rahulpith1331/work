pragma solidity ^0.8.0;

contract overloading{

    function calculate(uint a, uint b) public pure returns(uint){
     return a+b;   
    }

    function calculate(uint a, uint b, uint c) public pure returns(uint){
        return a + b + c;
    }
}