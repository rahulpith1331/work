pragma solidity ^0.8.0;

contract Base {
 uint inputData ;
 uint OutputData ;

 constructor(uint _input, uint _output)public{
     inputData = _input;
     OutputData = _output;
 }
}

contract Derive is Base{

    constructor(uint In, uint Out) Base(In, Out) public{}

    function getOutput() public view returns(uint){
        return OutputData;
    }
}