pragma solidity ^0.8.0;

contract Array{

    uint256[] public  myArray;

    function push(uint _number) public {
        myArray.push(_number);
    }

    function pop() public {
        myArray.pop();
    }

    function remove(uint _i) public {
      for(uint i = _i; i < myArray.length-1; i++){
       myArray[i] = myArray[i+1];
      }
       myArray.pop();
    }

    function length() public view returns(uint){
        return myArray.length;
    }
}