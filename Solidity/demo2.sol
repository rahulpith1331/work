pragma solidity ^0.8.0;

import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract DemoReentrancyGuard is Ownable, ERC20, ReentrancyGuard{
    constructor() ERC20("MyToken", "MTK") {
        _mint(msg.sender, 1000 * 10 ** decimals());
    }
    function _beforeTokenTransfer(address from, address to, uint256 amount)
        internal
        nonReentrant
        override
    {
        super._beforeTokenTransfer(from, to, amount);
    }
}
