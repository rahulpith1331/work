const router = require('express').Router();
const controllers = require('./lib/controllers');

router.patch('/editprofile', controllers.editProfile);
router.post('/getuser', controllers.getUserData);
router.post('/checkuser', controllers.checkUserExist);

module.exports = router;
