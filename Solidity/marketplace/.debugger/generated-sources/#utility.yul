{

    function abi_decode_t_address(offset, end) -> value {
        value := calldataload(offset)
        validator_revert_t_address(value)
    }

    function abi_decode_t_bool(offset, end) -> value {
        value := calldataload(offset)
        validator_revert_t_bool(value)
    }

    function abi_decode_t_enum$__tokentypes_$3350(offset, end) -> value {
        value := calldataload(offset)
        validator_revert_t_enum$__tokentypes_$3350(value)
    }

    function abi_decode_t_uint256(offset, end) -> value {
        value := calldataload(offset)
        validator_revert_t_uint256(value)
    }

    function abi_decode_t_uint256_fromMemory(offset, end) -> value {
        value := mload(offset)
        validator_revert_t_uint256(value)
    }

    function abi_decode_tuple_t_address(headStart, dataEnd) -> value0 {
        if slt(sub(dataEnd, headStart), 32) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_address(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_addresst_enum$__tokentypes_$3350t_uint256(headStart, dataEnd) -> value0, value1, value2 {
        if slt(sub(dataEnd, headStart), 96) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_address(add(headStart, offset), dataEnd)
        }

        {

            let offset := 32

            value1 := abi_decode_t_enum$__tokentypes_$3350(add(headStart, offset), dataEnd)
        }

        {

            let offset := 64

            value2 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_boolt_uint256(headStart, dataEnd) -> value0, value1 {
        if slt(sub(dataEnd, headStart), 64) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_bool(add(headStart, offset), dataEnd)
        }

        {

            let offset := 32

            value1 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_boolt_uint256t_uint256(headStart, dataEnd) -> value0, value1, value2 {
        if slt(sub(dataEnd, headStart), 96) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_bool(add(headStart, offset), dataEnd)
        }

        {

            let offset := 32

            value1 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

        {

            let offset := 64

            value2 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_boolt_uint256t_uint256t_uint256(headStart, dataEnd) -> value0, value1, value2, value3 {
        if slt(sub(dataEnd, headStart), 128) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_bool(add(headStart, offset), dataEnd)
        }

        {

            let offset := 32

            value1 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

        {

            let offset := 64

            value2 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

        {

            let offset := 96

            value3 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_enum$__tokentypes_$3350t_uint256(headStart, dataEnd) -> value0, value1 {
        if slt(sub(dataEnd, headStart), 64) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_enum$__tokentypes_$3350(add(headStart, offset), dataEnd)
        }

        {

            let offset := 32

            value1 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_uint256(headStart, dataEnd) -> value0 {
        if slt(sub(dataEnd, headStart), 32) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_uint256(add(headStart, offset), dataEnd)
        }

    }

    function abi_decode_tuple_t_uint256_fromMemory(headStart, dataEnd) -> value0 {
        if slt(sub(dataEnd, headStart), 32) { revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() }

        {

            let offset := 0

            value0 := abi_decode_t_uint256_fromMemory(add(headStart, offset), dataEnd)
        }

    }

    function abi_encode_t_address_to_t_address(value, pos) {
        mstore(pos, cleanup_t_address(value))
    }

    function abi_encode_t_address_to_t_address_fromStack(value, pos) {
        mstore(pos, cleanup_t_address(value))
    }

    function abi_encode_t_bool_to_t_bool_fromStack(value, pos) {
        mstore(pos, cleanup_t_bool(value))
    }

    function abi_encode_t_stringliteral_184b62af0b840852d96fd395f6f3b2b949df0842b9156a020d20f58eb8c022ab_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 39)
        store_literal_in_memory_184b62af0b840852d96fd395f6f3b2b949df0842b9156a020d20f58eb8c022ab(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_22f582700181ff311e55c1f2fcf2feaa87d53b93b06eae503e151e12821844b3_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 41)
        store_literal_in_memory_22f582700181ff311e55c1f2fcf2feaa87d53b93b06eae503e151e12821844b3(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_245f15ff17f551913a7a18385165551503906a406f905ac1c2437281a7cd0cfe_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 38)
        store_literal_in_memory_245f15ff17f551913a7a18385165551503906a406f905ac1c2437281a7cd0cfe(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_2704a526e6fc9c3e0cd7f9589d04231261080bd242978f351f6b3058d2da6947_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 42)
        store_literal_in_memory_2704a526e6fc9c3e0cd7f9589d04231261080bd242978f351f6b3058d2da6947(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_2cefede5a9ef0465af57321b7b8073d5e0c559531ccd438047cfb48f426837d5_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 68)
        store_literal_in_memory_2cefede5a9ef0465af57321b7b8073d5e0c559531ccd438047cfb48f426837d5(pos)
        end := add(pos, 96)
    }

    function abi_encode_t_stringliteral_4d885eb70b86cf92747ee1b564173967816e745e113ae70cf88790c548b86045_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 35)
        store_literal_in_memory_4d885eb70b86cf92747ee1b564173967816e745e113ae70cf88790c548b86045(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_4e67a1800e859fc6d7b0270f7d739c7d3241bfb5508194ebb04d018df9ee4104_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 45)
        store_literal_in_memory_4e67a1800e859fc6d7b0270f7d739c7d3241bfb5508194ebb04d018df9ee4104(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_5ff40b2abada337b58a2c36032a74cefbe58ce666c23319e3dc9ef30353b3f36_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 45)
        store_literal_in_memory_5ff40b2abada337b58a2c36032a74cefbe58ce666c23319e3dc9ef30353b3f36(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a_to_t_bytes_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_bytes_memory_ptr_fromStack(pos, 1)
        store_literal_in_memory_681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a(pos)
        end := add(pos, 32)
    }

    function abi_encode_t_stringliteral_6d3cea9520ad369251f7949a6088fd62a0420ff3d4be9d330bb18ebdf9026883_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 41)
        store_literal_in_memory_6d3cea9520ad369251f7949a6088fd62a0420ff3d4be9d330bb18ebdf9026883(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_7b3dd688e7c31f5a4b091c54dd9110287758dc086e60ae12d4a63026c3936667_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 36)
        store_literal_in_memory_7b3dd688e7c31f5a4b091c54dd9110287758dc086e60ae12d4a63026c3936667(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_871a2ff009f7dd0029cc92c45efc5223e6ff8388d83dbc2ded6ac7d4ad12e054_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 37)
        store_literal_in_memory_871a2ff009f7dd0029cc92c45efc5223e6ff8388d83dbc2ded6ac7d4ad12e054(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_9924ebdf1add33d25d4ef888e16131f0a5687b0580a36c21b5c301a6c462effe_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 32)
        store_literal_in_memory_9924ebdf1add33d25d4ef888e16131f0a5687b0580a36c21b5c301a6c462effe(pos)
        end := add(pos, 32)
    }

    function abi_encode_t_stringliteral_aeabd36f44b0ade4f112ed54a38b29e31b7cde037c65ade3e444cae7b0ea7a5a_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 61)
        store_literal_in_memory_aeabd36f44b0ade4f112ed54a38b29e31b7cde037c65ade3e444cae7b0ea7a5a(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_aee396938dbe418bc219995d71b706c43e17fa3ef9e18c77588d796720edf063_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 36)
        store_literal_in_memory_aee396938dbe418bc219995d71b706c43e17fa3ef9e18c77588d796720edf063(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_af5147a467bcab2061b35175067fe2c1798b3f1435d093ccb094316227dd92c0_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 42)
        store_literal_in_memory_af5147a467bcab2061b35175067fe2c1798b3f1435d093ccb094316227dd92c0(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_b093b090f04efe7da3f666b62caad1e5833fdf1300e172ccdde5ee15ae0afbf7_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 45)
        store_literal_in_memory_b093b090f04efe7da3f666b62caad1e5833fdf1300e172ccdde5ee15ae0afbf7(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_c3b43266416a28081889eba57014f7dd8a1d083ce7a615378e3c174118a84256_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 52)
        store_literal_in_memory_c3b43266416a28081889eba57014f7dd8a1d083ce7a615378e3c174118a84256(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470_to_t_bytes_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_bytes_memory_ptr_fromStack(pos, 0)
        store_literal_in_memory_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470(pos)
        end := add(pos, 0)
    }

    function abi_encode_t_stringliteral_d5aa9c19f897b00bfd74f37741e2203b8230c063308cced3d6902e2a38954676_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 35)
        store_literal_in_memory_d5aa9c19f897b00bfd74f37741e2203b8230c063308cced3d6902e2a38954676(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_e7ba6a2a64e8623798f1f7e12722b34804c6a5ca40ccaf458a2cad8e55ac04e4_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 41)
        store_literal_in_memory_e7ba6a2a64e8623798f1f7e12722b34804c6a5ca40ccaf458a2cad8e55ac04e4(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_eb39e8ee796c56ba94f1a9803993c4f66cff260ce2b9e09f09f297d539c1a16e_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 41)
        store_literal_in_memory_eb39e8ee796c56ba94f1a9803993c4f66cff260ce2b9e09f09f297d539c1a16e(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_f0aea88efdcef3574be2555017c5ec25f3e9b59982c061926de682eaf9d86e02_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 47)
        store_literal_in_memory_f0aea88efdcef3574be2555017c5ec25f3e9b59982c061926de682eaf9d86e02(pos)
        end := add(pos, 64)
    }

    function abi_encode_t_stringliteral_f19592c677a1e408346a7f61a4bd9e2ac95e4e85c0d542a648d0dbed81c5fe5d_to_t_string_memory_ptr_fromStack(pos) -> end {
        pos := array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, 40)
        store_literal_in_memory_f19592c677a1e408346a7f61a4bd9e2ac95e4e85c0d542a648d0dbed81c5fe5d(pos)
        end := add(pos, 64)
    }

    // struct MarketPlace.Bid -> struct MarketPlace.Bid
    function abi_encode_t_struct$_Bid_$3325_memory_ptr_to_t_struct$_Bid_$3325_memory_ptr_fromStack(value, pos)  {
        let tail := add(pos, 0x60)

        {
            // bider

            let memberValue0 := mload(add(value, 0x00))
            abi_encode_t_address_to_t_address(memberValue0, add(pos, 0x00))
        }

        {
            // bidamount

            let memberValue0 := mload(add(value, 0x20))
            abi_encode_t_uint256_to_t_uint256(memberValue0, add(pos, 0x20))
        }

        {
            // quantity

            let memberValue0 := mload(add(value, 0x40))
            abi_encode_t_uint256_to_t_uint256(memberValue0, add(pos, 0x40))
        }

    }

    function abi_encode_t_uint256_to_t_uint256(value, pos) {
        mstore(pos, cleanup_t_uint256(value))
    }

    function abi_encode_t_uint256_to_t_uint256_fromStack(value, pos) {
        mstore(pos, cleanup_t_uint256(value))
    }

    function abi_encode_tuple_t_address__to_t_address__fromStack_reversed(headStart , value0) -> tail {
        tail := add(headStart, 32)

        abi_encode_t_address_to_t_address_fromStack(value0,  add(headStart, 0))

    }

    function abi_encode_tuple_t_address_t_address_t_uint256_t_stringliteral_681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a__to_t_address_t_address_t_uint256_t_bytes_memory_ptr__fromStack_reversed(headStart , value2, value1, value0) -> tail {
        tail := add(headStart, 128)

        abi_encode_t_address_to_t_address_fromStack(value0,  add(headStart, 0))

        abi_encode_t_address_to_t_address_fromStack(value1,  add(headStart, 32))

        abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))

        mstore(add(headStart, 96), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a_to_t_bytes_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_address_t_address_t_uint256_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470__to_t_address_t_address_t_uint256_t_bytes_memory_ptr__fromStack_reversed(headStart , value2, value1, value0) -> tail {
        tail := add(headStart, 128)

        abi_encode_t_address_to_t_address_fromStack(value0,  add(headStart, 0))

        abi_encode_t_address_to_t_address_fromStack(value1,  add(headStart, 32))

        abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))

        mstore(add(headStart, 96), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470_to_t_bytes_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_address_t_address_t_uint256_t_uint256_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470__to_t_address_t_address_t_uint256_t_uint256_t_bytes_memory_ptr__fromStack_reversed(headStart , value3, value2, value1, value0) -> tail {
        tail := add(headStart, 160)

        abi_encode_t_address_to_t_address_fromStack(value0,  add(headStart, 0))

        abi_encode_t_address_to_t_address_fromStack(value1,  add(headStart, 32))

        abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))

        abi_encode_t_uint256_to_t_uint256_fromStack(value3,  add(headStart, 96))

        mstore(add(headStart, 128), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470_to_t_bytes_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_address_t_uint256__to_t_address_t_uint256__fromStack_reversed(headStart , value1, value0) -> tail {
        tail := add(headStart, 64)

        abi_encode_t_address_to_t_address_fromStack(value0,  add(headStart, 0))

        abi_encode_t_uint256_to_t_uint256_fromStack(value1,  add(headStart, 32))

    }

    function abi_encode_tuple_t_address_t_uint256_t_uint256_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470__to_t_address_t_uint256_t_uint256_t_bytes_memory_ptr__fromStack_reversed(headStart , value2, value1, value0) -> tail {
        tail := add(headStart, 128)

        abi_encode_t_address_to_t_address_fromStack(value0,  add(headStart, 0))

        abi_encode_t_uint256_to_t_uint256_fromStack(value1,  add(headStart, 32))

        abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))

        mstore(add(headStart, 96), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470_to_t_bytes_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_bool__to_t_bool__fromStack_reversed(headStart , value0) -> tail {
        tail := add(headStart, 32)

        abi_encode_t_bool_to_t_bool_fromStack(value0,  add(headStart, 0))

    }

    function abi_encode_tuple_t_stringliteral_184b62af0b840852d96fd395f6f3b2b949df0842b9156a020d20f58eb8c022ab__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_184b62af0b840852d96fd395f6f3b2b949df0842b9156a020d20f58eb8c022ab_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_22f582700181ff311e55c1f2fcf2feaa87d53b93b06eae503e151e12821844b3__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_22f582700181ff311e55c1f2fcf2feaa87d53b93b06eae503e151e12821844b3_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_245f15ff17f551913a7a18385165551503906a406f905ac1c2437281a7cd0cfe__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_245f15ff17f551913a7a18385165551503906a406f905ac1c2437281a7cd0cfe_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_2704a526e6fc9c3e0cd7f9589d04231261080bd242978f351f6b3058d2da6947__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_2704a526e6fc9c3e0cd7f9589d04231261080bd242978f351f6b3058d2da6947_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_2cefede5a9ef0465af57321b7b8073d5e0c559531ccd438047cfb48f426837d5__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_2cefede5a9ef0465af57321b7b8073d5e0c559531ccd438047cfb48f426837d5_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_4d885eb70b86cf92747ee1b564173967816e745e113ae70cf88790c548b86045__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_4d885eb70b86cf92747ee1b564173967816e745e113ae70cf88790c548b86045_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_4e67a1800e859fc6d7b0270f7d739c7d3241bfb5508194ebb04d018df9ee4104__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_4e67a1800e859fc6d7b0270f7d739c7d3241bfb5508194ebb04d018df9ee4104_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_5ff40b2abada337b58a2c36032a74cefbe58ce666c23319e3dc9ef30353b3f36__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_5ff40b2abada337b58a2c36032a74cefbe58ce666c23319e3dc9ef30353b3f36_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_6d3cea9520ad369251f7949a6088fd62a0420ff3d4be9d330bb18ebdf9026883__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_6d3cea9520ad369251f7949a6088fd62a0420ff3d4be9d330bb18ebdf9026883_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_7b3dd688e7c31f5a4b091c54dd9110287758dc086e60ae12d4a63026c3936667__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_7b3dd688e7c31f5a4b091c54dd9110287758dc086e60ae12d4a63026c3936667_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_871a2ff009f7dd0029cc92c45efc5223e6ff8388d83dbc2ded6ac7d4ad12e054__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_871a2ff009f7dd0029cc92c45efc5223e6ff8388d83dbc2ded6ac7d4ad12e054_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_9924ebdf1add33d25d4ef888e16131f0a5687b0580a36c21b5c301a6c462effe__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_9924ebdf1add33d25d4ef888e16131f0a5687b0580a36c21b5c301a6c462effe_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_aeabd36f44b0ade4f112ed54a38b29e31b7cde037c65ade3e444cae7b0ea7a5a__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_aeabd36f44b0ade4f112ed54a38b29e31b7cde037c65ade3e444cae7b0ea7a5a_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_aee396938dbe418bc219995d71b706c43e17fa3ef9e18c77588d796720edf063__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_aee396938dbe418bc219995d71b706c43e17fa3ef9e18c77588d796720edf063_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_af5147a467bcab2061b35175067fe2c1798b3f1435d093ccb094316227dd92c0__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_af5147a467bcab2061b35175067fe2c1798b3f1435d093ccb094316227dd92c0_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_b093b090f04efe7da3f666b62caad1e5833fdf1300e172ccdde5ee15ae0afbf7__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_b093b090f04efe7da3f666b62caad1e5833fdf1300e172ccdde5ee15ae0afbf7_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_c3b43266416a28081889eba57014f7dd8a1d083ce7a615378e3c174118a84256__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_c3b43266416a28081889eba57014f7dd8a1d083ce7a615378e3c174118a84256_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_d5aa9c19f897b00bfd74f37741e2203b8230c063308cced3d6902e2a38954676__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_d5aa9c19f897b00bfd74f37741e2203b8230c063308cced3d6902e2a38954676_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_e7ba6a2a64e8623798f1f7e12722b34804c6a5ca40ccaf458a2cad8e55ac04e4__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_e7ba6a2a64e8623798f1f7e12722b34804c6a5ca40ccaf458a2cad8e55ac04e4_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_eb39e8ee796c56ba94f1a9803993c4f66cff260ce2b9e09f09f297d539c1a16e__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_eb39e8ee796c56ba94f1a9803993c4f66cff260ce2b9e09f09f297d539c1a16e_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_f0aea88efdcef3574be2555017c5ec25f3e9b59982c061926de682eaf9d86e02__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_f0aea88efdcef3574be2555017c5ec25f3e9b59982c061926de682eaf9d86e02_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_stringliteral_f19592c677a1e408346a7f61a4bd9e2ac95e4e85c0d542a648d0dbed81c5fe5d__to_t_string_memory_ptr__fromStack_reversed(headStart ) -> tail {
        tail := add(headStart, 32)

        mstore(add(headStart, 0), sub(tail, headStart))
        tail := abi_encode_t_stringliteral_f19592c677a1e408346a7f61a4bd9e2ac95e4e85c0d542a648d0dbed81c5fe5d_to_t_string_memory_ptr_fromStack( tail)

    }

    function abi_encode_tuple_t_struct$_Bid_$3325_memory_ptr__to_t_struct$_Bid_$3325_memory_ptr__fromStack_reversed(headStart , value0) -> tail {
        tail := add(headStart, 96)

        abi_encode_t_struct$_Bid_$3325_memory_ptr_to_t_struct$_Bid_$3325_memory_ptr_fromStack(value0,  add(headStart, 0))

    }

    function abi_encode_tuple_t_uint256__to_t_uint256__fromStack_reversed(headStart , value0) -> tail {
        tail := add(headStart, 32)

        abi_encode_t_uint256_to_t_uint256_fromStack(value0,  add(headStart, 0))

    }

    function abi_encode_tuple_t_uint256_t_uint256__to_t_uint256_t_uint256__fromStack_reversed(headStart , value1, value0) -> tail {
        tail := add(headStart, 64)

        abi_encode_t_uint256_to_t_uint256_fromStack(value0,  add(headStart, 0))

        abi_encode_t_uint256_to_t_uint256_fromStack(value1,  add(headStart, 32))

    }

    function abi_encode_tuple_t_uint256_t_uint256_t_uint256__to_t_uint256_t_uint256_t_uint256__fromStack_reversed(headStart , value2, value1, value0) -> tail {
        tail := add(headStart, 96)

        abi_encode_t_uint256_to_t_uint256_fromStack(value0,  add(headStart, 0))

        abi_encode_t_uint256_to_t_uint256_fromStack(value1,  add(headStart, 32))

        abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))

    }

    function abi_encode_tuple_t_uint256_t_uint256_t_uint256_t_uint256__to_t_uint256_t_uint256_t_uint256_t_uint256__fromStack_reversed(headStart , value3, value2, value1, value0) -> tail {
        tail := add(headStart, 128)

        abi_encode_t_uint256_to_t_uint256_fromStack(value0,  add(headStart, 0))

        abi_encode_t_uint256_to_t_uint256_fromStack(value1,  add(headStart, 32))

        abi_encode_t_uint256_to_t_uint256_fromStack(value2,  add(headStart, 64))

        abi_encode_t_uint256_to_t_uint256_fromStack(value3,  add(headStart, 96))

    }

    function allocate_unbounded() -> memPtr {
        memPtr := mload(64)
    }

    function array_storeLengthForEncoding_t_bytes_memory_ptr_fromStack(pos, length) -> updated_pos {
        mstore(pos, length)
        updated_pos := add(pos, 0x20)
    }

    function array_storeLengthForEncoding_t_string_memory_ptr_fromStack(pos, length) -> updated_pos {
        mstore(pos, length)
        updated_pos := add(pos, 0x20)
    }

    function checked_add_t_uint256(x, y) -> sum {
        x := cleanup_t_uint256(x)
        y := cleanup_t_uint256(y)

        // overflow, if x > (maxValue - y)
        if gt(x, sub(0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff, y)) { panic_error_0x11() }

        sum := add(x, y)
    }

    function checked_mul_t_uint256(x, y) -> product {
        x := cleanup_t_uint256(x)
        y := cleanup_t_uint256(y)

        // overflow, if x != 0 and y > (maxValue / x)
        if and(iszero(iszero(x)), gt(y, div(0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff, x))) { panic_error_0x11() }

        product := mul(x, y)
    }

    function checked_sub_t_uint256(x, y) -> diff {
        x := cleanup_t_uint256(x)
        y := cleanup_t_uint256(y)

        if lt(x, y) { panic_error_0x11() }

        diff := sub(x, y)
    }

    function cleanup_t_address(value) -> cleaned {
        cleaned := cleanup_t_uint160(value)
    }

    function cleanup_t_bool(value) -> cleaned {
        cleaned := iszero(iszero(value))
    }

    function cleanup_t_uint160(value) -> cleaned {
        cleaned := and(value, 0xffffffffffffffffffffffffffffffffffffffff)
    }

    function cleanup_t_uint256(value) -> cleaned {
        cleaned := value
    }

    function increment_t_uint256(value) -> ret {
        value := cleanup_t_uint256(value)
        if eq(value, 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff) { panic_error_0x11() }
        ret := add(value, 1)
    }

    function panic_error_0x11() {
        mstore(0, 35408467139433450592217433187231851964531694900788300625387963629091585785856)
        mstore(4, 0x11)
        revert(0, 0x24)
    }

    function panic_error_0x21() {
        mstore(0, 35408467139433450592217433187231851964531694900788300625387963629091585785856)
        mstore(4, 0x21)
        revert(0, 0x24)
    }

    function revert_error_c1322bf8034eace5e0b5c7295db60986aa89aae5e0ea0873e4689e076861a5db() {
        revert(0, 0)
    }

    function revert_error_dbdddcbe895c83990c08b3492a0e83918d802a52331272ac6fdb6a7c4aea3b1b() {
        revert(0, 0)
    }

    function store_literal_in_memory_184b62af0b840852d96fd395f6f3b2b949df0842b9156a020d20f58eb8c022ab(memPtr) {

        mstore(add(memPtr, 0), "Market Place: already listed on ")

        mstore(add(memPtr, 32), "auction")

    }

    function store_literal_in_memory_22f582700181ff311e55c1f2fcf2feaa87d53b93b06eae503e151e12821844b3(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Token not out for ")

        mstore(add(memPtr, 32), "sell yet.")

    }

    function store_literal_in_memory_245f15ff17f551913a7a18385165551503906a406f905ac1c2437281a7cd0cfe(memPtr) {

        mstore(add(memPtr, 0), "Ownable: new owner is the zero a")

        mstore(add(memPtr, 32), "ddress")

    }

    function store_literal_in_memory_2704a526e6fc9c3e0cd7f9589d04231261080bd242978f351f6b3058d2da6947(memPtr) {

        mstore(add(memPtr, 0), "marketplace: Insufficent quantit")

        mstore(add(memPtr, 32), "y for sell")

    }

    function store_literal_in_memory_2cefede5a9ef0465af57321b7b8073d5e0c559531ccd438047cfb48f426837d5(memPtr) {

        mstore(add(memPtr, 0), "MarketPlace: Select ERC1155 to m")

        mstore(add(memPtr, 32), "int multiple copy of single toke")

        mstore(add(memPtr, 64), "n id")

    }

    function store_literal_in_memory_4d885eb70b86cf92747ee1b564173967816e745e113ae70cf88790c548b86045(memPtr) {

        mstore(add(memPtr, 0), "MarketPlace: Owner can't buy tok")

        mstore(add(memPtr, 32), "en ")

    }

    function store_literal_in_memory_4e67a1800e859fc6d7b0270f7d739c7d3241bfb5508194ebb04d018df9ee4104(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Zero addresses are")

        mstore(add(memPtr, 32), " not allowed.")

    }

    function store_literal_in_memory_5ff40b2abada337b58a2c36032a74cefbe58ce666c23319e3dc9ef30353b3f36(memPtr) {

        mstore(add(memPtr, 0), "marketplace: Insufficent quantit")

        mstore(add(memPtr, 32), "y for auction")

    }

    function store_literal_in_memory_681afa780d17da29203322b473d3f210a7d621259a4e6ce9e403f5a266ff719a(memPtr) {

        mstore(add(memPtr, 0), " ")

    }

    function store_literal_in_memory_6d3cea9520ad369251f7949a6088fd62a0420ff3d4be9d330bb18ebdf9026883(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Token id is not mi")

        mstore(add(memPtr, 32), "nted yet.")

    }

    function store_literal_in_memory_7b3dd688e7c31f5a4b091c54dd9110287758dc086e60ae12d4a63026c3936667(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Already listed on ")

        mstore(add(memPtr, 32), "sell")

    }

    function store_literal_in_memory_871a2ff009f7dd0029cc92c45efc5223e6ff8388d83dbc2ded6ac7d4ad12e054(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Auction isn't ende")

        mstore(add(memPtr, 32), "d yet")

    }

    function store_literal_in_memory_9924ebdf1add33d25d4ef888e16131f0a5687b0580a36c21b5c301a6c462effe(memPtr) {

        mstore(add(memPtr, 0), "Ownable: caller is not the owner")

    }

    function store_literal_in_memory_aeabd36f44b0ade4f112ed54a38b29e31b7cde037c65ade3e444cae7b0ea7a5a(memPtr) {

        mstore(add(memPtr, 0), "MarketPlace: Select ERC1155 to a")

        mstore(add(memPtr, 32), "uction more than one quantity")

    }

    function store_literal_in_memory_aee396938dbe418bc219995d71b706c43e17fa3ef9e18c77588d796720edf063(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Sorry auction is e")

        mstore(add(memPtr, 32), "nded")

    }

    function store_literal_in_memory_af5147a467bcab2061b35175067fe2c1798b3f1435d093ccb094316227dd92c0(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Not entered proper")

        mstore(add(memPtr, 32), " quantity.")

    }

    function store_literal_in_memory_b093b090f04efe7da3f666b62caad1e5833fdf1300e172ccdde5ee15ae0afbf7(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Price shouldn't be")

        mstore(add(memPtr, 32), " zero or less")

    }

    function store_literal_in_memory_c3b43266416a28081889eba57014f7dd8a1d083ce7a615378e3c174118a84256(memPtr) {

        mstore(add(memPtr, 0), "Market  Place: Bid should be hig")

        mstore(add(memPtr, 32), "her than initial bid")

    }

    function store_literal_in_memory_c5d2460186f7233c927e7db2dcc703c0e500b653ca82273b7bfad8045d85a470(memPtr) {

    }

    function store_literal_in_memory_d5aa9c19f897b00bfd74f37741e2203b8230c063308cced3d6902e2a38954676(memPtr) {

        mstore(add(memPtr, 0), "MarketPlace: Owner can't bid tok")

        mstore(add(memPtr, 32), "en ")

    }

    function store_literal_in_memory_e7ba6a2a64e8623798f1f7e12722b34804c6a5ca40ccaf458a2cad8e55ac04e4(memPtr) {

        mstore(add(memPtr, 0), "MarketPlace: Enter proper quanti")

        mstore(add(memPtr, 32), "ty to buy")

    }

    function store_literal_in_memory_eb39e8ee796c56ba94f1a9803993c4f66cff260ce2b9e09f09f297d539c1a16e(memPtr) {

        mstore(add(memPtr, 0), "Market Place: you are not a high")

        mstore(add(memPtr, 32), "est bider")

    }

    function store_literal_in_memory_f0aea88efdcef3574be2555017c5ec25f3e9b59982c061926de682eaf9d86e02(memPtr) {

        mstore(add(memPtr, 0), "MarketPlace: Auction for this to")

        mstore(add(memPtr, 32), "ken has started")

    }

    function store_literal_in_memory_f19592c677a1e408346a7f61a4bd9e2ac95e4e85c0d542a648d0dbed81c5fe5d(memPtr) {

        mstore(add(memPtr, 0), "Market Place: Token quantity ins")

        mstore(add(memPtr, 32), "ufficent")

    }

    function validator_revert_t_address(value) {
        if iszero(eq(value, cleanup_t_address(value))) { revert(0, 0) }
    }

    function validator_revert_t_bool(value) {
        if iszero(eq(value, cleanup_t_bool(value))) { revert(0, 0) }
    }

    function validator_revert_t_enum$__tokentypes_$3350(value) {
        if iszero(lt(value, 2)) { revert(0, 0) }
    }

    function validator_revert_t_uint256(value) {
        if iszero(eq(value, cleanup_t_uint256(value))) { revert(0, 0) }
    }

}
