/// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract ERC1155Minter is ERC1155, Ownable {
    constructor(string memory _uri) ERC1155(_uri) {}

    function setURI(string memory newuri) public onlyOwner {
        _setURI(newuri);
    }

    function mint(address account, uint256 id, uint256 amount, bytes memory data)public {
        require(msg.sender!= tx.origin,"ERC721Minter: Only can mint this token.");
        _mint(account, id, amount, data);
    }

    function mintBatch(address to, uint256[] memory ids, uint256[] memory amounts, bytes memory data) public {
        require(msg.sender!= tx.origin,"ERC721Minter: Only can mint this token.");
        _mintBatch(to, ids, amounts, data);
    }

    function tokenTransferFrom(address from, address to, uint tokenid, uint amount) public {
        _safeTransferFrom(from, to, tokenid, amount, "");
    }
    
}