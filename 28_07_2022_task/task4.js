function grade(arr){
    var n = parseInt(readLine());
    for(var a0 = 0; a0 < n; a0++){
        var grade = parseInt(readLine());
        var incrementedGrade = grade;
        if(grade < 38){
            console.log(grade);
            continue;
        }
        // increment , if needed, until grade is a multiple of 5
        while(incrementedGrade % 5 != 0){
            incrementedGrade++
        }
        if(incrementedGrade - grade < 3 )
            console.log(incrementedGrade);
        else
            console.log(grade);
            
    }
}